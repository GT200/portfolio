#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##########################################################################
#    TP - Introduction à l'interpolation spatiale et aux géostatistiques #
##########################################################################

# P. Bosser / ENSTA Bretagne
# Version du 13/03/2022


# Numpy
import numpy as np
# Matplotlib / plot
import matplotlib.pyplot as plt
# lib geostatistic
import lib_gs as gs

# Chargement des données
data = np.loadtxt('points.dat')
# On force le format des données (matrice d'une colonne)
x_obs = data[:,0:1]
y_obs = data[:,1:2]
z_obs = data[:,2:3]

# Visualisation des données en entrée : sites de mesure
gs.plot_points(x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = "sites d'observation")
# Visualisation des données en entrée : sites de mesure coloré en fonction de la VR
gs.plot_patch(x_obs, y_obs, z_obs, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "observations")

# Création d'une grille planimétrique pour l'interpolation
x_grd, y_grd = np.meshgrid(np.linspace(np.floor(np.min(x_obs)), np.ceil(np.max(x_obs)), 100), np.linspace(np.floor(np.min(y_obs)), np.ceil(np.max(y_obs)), 100))

# Interpolation par plus proche voisin (PPV)
z__grd_int = gs.interp_ppv(x_obs, y_obs, z_obs, x_grd, y_grd)

# Visualiation de l'interpolation PPV : lignes de niveau
gs.plot_contour_2d(x_grd, y_grd, z__grd_int, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'PPV')

# Visualiation de l'interpolation PPV : surface colorée
gs.plot_surface_2d(x_grd, y_grd, z__grd_int, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'PPV')

# Interpolation en un point de l'espace
zi = gs.interp_ppv(x_obs, y_obs, z_obs, np.array([[225]]),  np.array([[180]]))
print("La valeur interpolée en (225,180) est "+str(zi))
plt.show()


############# interpolation linéaire ###########

z_int_lin = gs.interp_lin(x_obs, y_obs, z_obs, x_grd, y_grd)
            
# Visualiation de l'interpolation PPV : surface colorée
gs.plot_surface_2d(x_grd, y_grd, z_int_lin, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'LIN')

gs.plot_contour_2d(x_grd, y_grd, z_int_lin, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'LIN2')
  
        
############# interpolation  par inverse des distances à différentes puissances  ###########
 

    
z_inv_dist = gs.interp_inv_dist(x_obs, y_obs, z_obs, x_grd, y_grd, 2)
        
gs.plot_surface_2d(x_grd, y_grd, z_inv_dist, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'distances')

gs.plot_contour_2d(x_grd, y_grd, z_inv_dist, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'distances')
        
plt.show()
## interp_spl


    
z_int_spl = gs.interp_spl(x_obs, y_obs, z_obs, x_grd, y_grd, 0)
        
        
gs.plot_surface_2d(x_grd, y_grd, z_int_spl, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'z_int_spl')

gs.plot_contour_2d(x_grd, y_grd, z_int_spl, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'z_int_spl')
     

### Methode stochastique
    
dist, delta = gs.nuee(x_obs, y_obs, z_obs)
    
gs.showScatter(dist, delta)



(list_dist_moy, list_gamma) = gs.vario(dist, 500, delta)

gs.showScatter(list_dist_moy, list_gamma)


################### Modèle analitique

indice_co = np.where(list_gamma == np.max(list_gamma))

c0 = np.take(list_gamma, indice_co) 

a0 = np.take(list_dist_moy, indice_co, )

z_int_kr, new_gamma = gs.interp_krg(x_obs, y_obs, z_obs, x_grd, y_grd, c0,a0, list_dist_moy, list_gamma, 0.0001) 


plt.figure()

plt.scatter(dist, delta)
    
plt.plot(list_dist_moy, np.array(new_gamma), color="yellow")

plt.scatter(list_dist_moy, list_gamma, color="red")

plt.show()
            
        
gs.plot_surface_2d(x_grd, y_grd, z_int_kr, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'z_int_kr')

gs.plot_contour_2d(x_grd, y_grd, z_int_kr, x_obs, y_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = 'z_int_kr')
     
        
        
        
        



