#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##########################################################################
#    TP - Introduction à l'interpolation spatiale et aux géostatistiques #
##########################################################################

# P. Bosser 
# Tiazara Gaetan / Humbert Sarah
# Version du 16/03/2022


import numpy as np
import matplotlib.pyplot as plt
import math as m



####################### Fonctions d'interpolation ######################

def interp_lin(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation linéaire
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    from scipy.spatial import Delaunay as delaunay
    z_int = np.nan*np.zeros(x_int.shape)
    tri = delaunay(np.hstack((x_obs,y_obs)))
    
    ligne = np.shape(x_int)[0]
    colonne = np.shape(x_int)[1]
    
    for i in range(ligne):
        for j in range(colonne):
            x = x_int[i,j]
            y = y_int[i,j]
    
            # Recherche de l’index du triangle contenant le point x, y
            idx_t = tri.find_simplex( np.array([x, y]) )
            
            if idx_t != -1:
            
    
                # Recherche des index des sommets du triangle contenant le point x, y
                idx_s = tri.simplices[idx_t,:]
    
                # Coordonnées des sommets du triangle contenant le point x, y
                x1 = x_obs[ idx_s[0] ] ; y1 = y_obs[ idx_s[0] ] ; z1 = z_obs[ idx_s[0] ]
                x2 = x_obs[ idx_s[1] ] ; y2 = y_obs[ idx_s[1] ] ; z2 = z_obs[ idx_s[1] ]
                x3 = x_obs[ idx_s[2] ] ; y3 = y_obs[ idx_s[2] ] ; z3 = z_obs[ idx_s[2] ]
    
                A = np.array([[ float(x1), float(y1), 1],[ float(x2), float(y2), 1],[ float(x3), float(y3), 1]])
                B = np.array([[float(z1)], [float(z2)], [float(z3)]])
                X = np.linalg.solve(A, B)
    
                alpha = X[0,0]
                beta = X[1,0]
                gamma = X[2,0]
    
                z = alpha*x + beta*y + gamma
                z_int[i,j] = z
    
    
    return z_int



def distance(x1,y1,x2,y2):
    return m.sqrt( (x1-x2)**2 + (y1-y2)**2 )


def interp_inv(x_obs, y_obs, z_obs, x_int, y_int, p):
    # Interpolation par inverse des distances
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    z_int = np.nan*np.zeros(x_int.shape)
    
    ligne = np.shape(x_int)[0]
    colonne = np.shape(x_int)[1]
    n = np.shape(x_obs)[0]
    
    for i in range(ligne):
        for j in range(colonne):
            
            x2 = x_int[i,j]
            y2 = y_int[i,j]
            numerateur = 0
            denominateur = 0
    
    
            for k in range(n):
                x1 = x_obs[k,0]
                y1 = y_obs[k,0]
                z1 = z_obs[k,0]
            
                numerateur += ( z1 / distance(x1,y1,x2,y2)**p )
                denominateur += ( 1 / distance(x1,y1,x2,y2)**p )
            
            z_int[i,j] = numerateur / denominateur
            
    return z_int



def fonction_phi(x1,y1,x2,y2):
    r = distance(x1,y1,x2,y2)
    return (r**2)*np.log(r)



def interp_spl(x_obs, y_obs, z_obs, x_int, y_int, rho):
    # Interpolation par splines
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    z_int = np.nan*np.zeros(x_int.shape)
    n = np.shape(x_obs)[0]
    
    A1 = np.zeros((n+3,3))
    for i in range(n):
        A1[i,0] = 1
        A1[i,1] = x_obs[i,0]
        A1[i,2] = y_obs[i,0]
    
    A2 = np.zeros((n+3,n))
    for j in range(n):
        x1 = x_obs[j,0]
        y1 = y_obs[j,0]      
        for i in range(n):
            if i == j:
                A2[i,i] = rho
            else:
                x2 = x_obs[i,0]
                y2 = y_obs[i,0]
                A2[i,j] = fonction_phi(x1,y1,x2,y2)
    for k in range(n):
        A2[n,k] = 1
        A2[n+1,k] = x_obs[k,0]
        A2[n+2,k] = y_obs[k,0]
    
    A = np.hstack((A1,A2))
    
    B = np.zeros((n+3,1))
    for i in range(n):
        B[i,0] = z_obs[i,0]
    
    X = np.linalg.solve(A, B)
    
    ligne = np.shape(x_int)[0]
    colonne = np.shape(x_int)[1]
    n = np.shape(x_obs)[0]
    
    for i in range(ligne):
        for j in range(colonne):
            x = x_int[i,j]
            y = y_int[i,j]
    
            z = X[0,0] + X[1,0]*x + X[2,0]*y
            for k in range(n):
                b = X[3+k,0]
                x2 = x_obs[k,0]
                y2 = y_obs[k,0]
                z += b*fonction_phi(x,y,x2,y2)
                
            z_int[i,j] = z
            
    return z_int































############################# Visualisation ############################

def plot_contour_2d(x_grd ,y_grd ,z_grd, x_obs = np.array([]) ,y_obs = np.array([]), xlabel = "", ylabel = "", title = "", fileo = ""):
    # Tracé du champ interpolé sous forme d'isolignes
    # x_grd, y_grd, z_grd : grille de valeurs interpolées
    # x_obs, y_obs : observations (facultatif)
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    
    z_grd_m = np.ma.masked_invalid(z_grd)
    fig = plt.figure()
    plt.contour(x_grd, y_grd, z_grd_m, int(np.round((np.max(z_grd_m)-np.min(z_grd_m))/4)),colors ='k')
    if x_obs.shape[0]>0:
        plt.scatter(x_obs, y_obs, marker = 'o', c = 'k', s = 5)
        plt.xlim(0.95*np.min(x_obs),np.max(x_obs)+0.05*np.min(x_obs))
        plt.ylim(0.95*np.min(y_obs),np.max(y_obs)+0.05*np.min(y_obs))
    else:
        plt.xlim(0.95*np.min(x_grd),np.max(x_grd)+0.05*np.min(x_grd))
        plt.ylim(0.95*np.min(y_grd),np.max(y_grd)+0.05*np.min(y_grd))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_surface_2d(x_grd ,y_grd ,z_grd, x_obs = np.array([]) ,y_obs = np.array([]), minmax = [0,0], xlabel = "", ylabel = "", zlabel = "", title = "", fileo = ""):
    # Tracé du champ interpolé sous forme d'une surface colorée
    # x_grd, y_grd, z_grd : grille de valeurs interpolées
    # x_obs, y_obs : observations (facultatif)
    # minmax : valeurs min et max de la variable interpolée (facultatif)
    # xlabel, ylabel, zlabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    
    from matplotlib import cm
    
    z_grd_m = np.ma.masked_invalid(z_grd)
    fig = plt.figure()
    if minmax[0] < minmax[-1]:
        p=plt.pcolormesh(x_grd, y_grd, z_grd_m, cmap=cm.terrain, vmin = minmax[0], vmax = minmax[-1], shading = 'auto')
    else:
        p=plt.pcolormesh(x_grd, y_grd, z_grd_m, cmap=cm.terrain, shading = 'auto')
    if x_obs.shape[0]>0:
        plt.scatter(x_obs, y_obs, marker = 'o', c = 'k', s = 5)
        plt.xlim(0.95*np.min(x_obs),np.max(x_obs)+0.05*np.min(x_obs))
        plt.ylim(0.95*np.min(y_obs),np.max(y_obs)+0.05*np.min(y_obs))
    else:
        plt.xlim(0.95*np.min(x_grd),np.max(x_grd)+0.05*np.min(x_grd))
        plt.ylim(0.95*np.min(y_grd),np.max(y_grd)+0.05*np.min(y_grd))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    fig.colorbar(p,ax=plt.gca(),label=zlabel,fraction=0.046, pad=0.04)
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_points(x_obs, y_obs, xlabel = "", ylabel = "", title = "", fileo = ""):
    # Tracé des sites d'observations
    # x_obs, y_obs : observations
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    
    fig = plt.figure()
    ax = plt.gca()
    plt.plot(x_obs, y_obs, 'ok', ms = 4)
    ax.set_xlim(0.95*min(x_obs),max(x_obs)+0.05*min(x_obs))
    ax.set_ylim(0.95*min(y_obs),max(y_obs)+0.05*min(y_obs))
    ax.set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_patch(x_obs, y_obs, z_obs, xlabel = "", ylabel = "", zlabel = "", title = "", fileo = ""):
    # Tracé des valeurs observées
    # x_obs, y_obs, z_obs : observations
    # xlabel, ylabel, zlabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    
    from matplotlib import cm
    
    fig = plt.figure()
    p=plt.scatter(x_obs, y_obs, marker = 'o', c = z_obs, s = 80, cmap=cm.terrain)
    plt.xlim(0.95*min(x_obs),max(x_obs)+0.05*min(x_obs))
    plt.ylim(0.95*min(y_obs),max(y_obs)+0.05*min(y_obs))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    fig.colorbar(p,ax=plt.gca(),label=zlabel,fraction=0.046, pad=0.04)
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_triangulation(x_obs, y_obs, xlabel = "", ylabel = "", title = "", fileo = ""):
    # Tracé de la triangulation sur des sites d'observations
    # x_obs, y_obs : observations
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    from scipy.spatial import Delaunay as delaunay
    tri = delaunay(np.hstack((x_obs,y_obs)))
    
    plt.figure()
    plt.triplot(x_obs[:,0], y_obs[:,0], tri.simplices)
    plt.plot(x_obs, y_obs, 'or', ms=4)
    plt.xlim(0.95*min(x_obs),max(x_obs)+0.05*min(x_obs))
    plt.ylim(0.95*min(y_obs),max(y_obs)+0.05*min(y_obs))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')
