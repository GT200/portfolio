#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##########################################################################
#    EVALUATION - Introduction à l'interpolation spatiale et aux géostatistiques #
##########################################################################

# Tiazara Geatan / Humbert Sarah
# Version du 16/03/2022

import matplotlib.path as mpltPath

# Numpy
import numpy as np
# Matplotlib / plot
import matplotlib.pyplot as plt
import math as m
# lib geostatistic
import lib_gs as gs



contour = np.loadtxt('FR_contour.txt')
polygone = mpltPath.Path(contour)

data = np.loadtxt('FR_T2M_20190629_120000.txt')
E_obs = data[:,0].reshape((-1, 1)) / 1000
N_obs = data[:,1].reshape((-1, 1)) / 1000
T_obs = data[:,2].reshape((-1, 1))

# points à interpoler
# Brest 147 6837
# Champs-sur-Marne 669 6860
# Forcalquier 917 6327

## Visualisation des données en entrée : sites de mesure
gs.plot_points(E_obs, N_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = "sites d'observation")
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()
## Visualisation des données en entrée : sites de mesure coloré en fonction de la VR
gs.plot_patch(E_obs, N_obs, T_obs, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'T [°C]', title = "observations")
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()
## Création d'une grille planimétrique pour l'interpolation
size_grd = 100
E_grd, N_grd = np.meshgrid(np.linspace(np.floor(np.min(E_obs)), np.ceil(np.max(E_obs)), size_grd), np.linspace(np.floor(np.min(N_obs)), np.ceil(np.max(N_obs)), size_grd))

################### Interpolation linéaire
z_interpolation_lineaire = gs.interp_lin(E_obs, N_obs, T_obs, E_grd, N_grd)

## retrait des zone interpolées en dehors du territoire
z_interpolation_lineaire_inside_coutour = gs.removeValueOutsideBorder(E_grd, N_grd, z_interpolation_lineaire, polygone)

## Visualiation de l'interpolation linéaire : lignes de niveau
gs.plot_contour_2d(E_grd, N_grd, z_interpolation_lineaire_inside_coutour, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'interpolation linéaire')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()
# Visualiation de l'interpolation linéaire : surface colorée
gs.plot_surface_2d(E_grd, N_grd, z_interpolation_lineaire_inside_coutour, E_obs, N_obs, minmax = [17,40], xlabel = 'E [km]', ylabel = 'N [km]', title = 'interpolation linéaire',zlabel = 'T [°C]' )
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()
## Interpolation en un point de l'espace
zi = gs.interp_lin(E_obs, N_obs, T_obs, np.array([[147]]),  np.array([[6837]]))
#print("La valeur interpolée à Brest est "+str(zi))
zi = gs.interp_lin(E_obs, N_obs, T_obs, np.array([[669]]),  np.array([[6860]]))
#print("La valeur interpolée à Champs-sur-Marne est "+str(zi))
zi = gs.interp_lin(E_obs, N_obs, T_obs, np.array([[917]]),  np.array([[6327]]))
#print("La valeur interpolée à Forcalquier est "+str(zi))

################### Interpolation inverse des distances
z_inv_4 = gs.interp_inv(E_obs, N_obs, T_obs, E_grd, N_grd, 4)

#z_inv_2 = gs.interp_inv(E_obs, N_obs, T_obs, E_grd, N_grd, 2)
## retrait des zones interpolées en dehors du territoire
z_interpolation_inv_4_inside_coutour = gs.removeValueOutsideBorder(E_grd, N_grd, z_inv_4, polygone)

## Visualiation de l'interpolation linéaire : lignes de niveau
gs.plot_contour_2d(E_grd, N_grd, z_interpolation_inv_4_inside_coutour, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'inverse des distances p4')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

# Visualiation de l'interpolation linéaire : surface colorée
gs.plot_surface_2d(E_grd, N_grd, z_interpolation_inv_4_inside_coutour, E_obs, N_obs, minmax = [17,40], xlabel = 'E [km]', ylabel = 'N [km]', title = 'inverse des distances p4', zlabel = 'T [°C]' )
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

## Interpolation en un point de l'espace
zi = gs.interp_inv(E_obs, N_obs, T_obs, np.array([[147]]),  np.array([[6837]]), 4)
#print("La valeur interpolée à Brest est "+str(zi))
zi = gs.interp_inv(E_obs, N_obs, T_obs, np.array([[669]]),  np.array([[6860]]), 4)
#print("La valeur interpolée à Champs-sur-Marne est "+str(zi))
zi = gs.interp_inv(E_obs, N_obs, T_obs, np.array([[917]]),  np.array([[6327]]), 4)
#print("La valeur interpolée à Forcalquier est "+str(zi))

################### Interpolation splines
z_spl0 = gs.interp_spl(E_obs, N_obs, T_obs, E_grd, N_grd, 0)

z_spl10P3 = gs.interp_spl(E_obs, N_obs, T_obs, E_grd, N_grd, 10**3)

z_spl10P6 = gs.interp_spl(E_obs, N_obs, T_obs, E_grd, N_grd, 10**6)

z_spl10P9 = gs.interp_spl(E_obs, N_obs, T_obs, E_grd, N_grd, 10**9)

## retrait des zones interpolées en dehors du territoire
z_interpolation_spl0_inside_coutour = gs.removeValueOutsideBorder(E_grd, N_grd, z_spl0, polygone)

z_interpolation_spl10P3_inside_coutour = gs.removeValueOutsideBorder(E_grd, N_grd, z_spl10P3, polygone)

z_interpolation_spl10P6_inside_coutour = gs.removeValueOutsideBorder(E_grd, N_grd, z_spl10P6, polygone)

z_interpolation_spl10P9_inside_coutour = gs.removeValueOutsideBorder(E_grd, N_grd, z_spl10P9, polygone)

## Visualiation splines de lissage : lignes de niveau
gs.plot_contour_2d(E_grd, N_grd, z_spl0, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'splines de lissage  rho 0')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

# Visualiation splines de lissage : surface colorée
gs.plot_surface_2d(E_grd, N_grd, z_spl0, E_obs, N_obs, minmax = [17,40], xlabel = 'E [km]', ylabel = 'N [km]', title = 'splines de lissage rho 0')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

# Visualiation splines de lissage : lignes de niveau
gs.plot_contour_2d(E_grd, N_grd, z_spl10P6, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'splines de lissage  rho 10**6')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

# Visualiation splines de lissage : surface colorée
gs.plot_surface_2d(E_grd, N_grd, z_spl10P6, E_obs, N_obs, minmax = [17,40], xlabel = 'E [km]', ylabel = 'N [km]', title = 'splines de lissage rho 10**6')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

gs.plot_contour_2d(E_grd, N_grd, z_spl10P9, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'splines de lissage  rho 10**9')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

# Visualiation splines de lissage : surface colorée
gs.plot_surface_2d(E_grd, N_grd, z_spl10P9, E_obs, N_obs, minmax = [17,40], xlabel = 'E [km]', ylabel = 'N [km]', title = 'splines de lissage rho 10**9')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

## Interpolation en un point de l'espace
zi = gs.interp_spl(E_obs, N_obs, T_obs, np.array([[147]]),  np.array([[6837]]), 1000000)
print("La valeur interpolée à Brest est "+str(zi))
zi = gs.interp_spl(E_obs, N_obs, T_obs, np.array([[669]]),  np.array([[6860]]), 1000000)
print("La valeur interpolée à Champs-sur-Marne est "+str(zi))
zi = gs.interp_spl(E_obs, N_obs, T_obs, np.array([[917]]),  np.array([[6327]]), 1000000)
print("La valeur interpolée à Forcalquier est "+str(zi))

################### Interpolation krigeage
##variogramme
dist, delta = gs.nuee(E_obs, N_obs, T_obs)

(list_dist_moy, list_gamma) = gs.vario(dist, 500, delta)

indice_co = np.where(list_gamma == np.max(list_gamma))

c0 = np.take(list_gamma, indice_co)

a0 = np.take(list_dist_moy, indice_co, )
##
z_int_kr_gausse, new_gamma = gs.interp_krg_gauss(E_obs, N_obs, T_obs, E_grd, N_grd, c0,a0, list_dist_moy, list_gamma, 0.0001)

##afficahge nueé et variographe
plt.figure()

plt.scatter(dist, delta)

plt.plot(list_dist_moy, np.array(new_gamma), color="orange")

plt.scatter(list_dist_moy, list_gamma, color="red")

plt.show()

## retrait des zones interpolées en dehors du territoire
z_int_kr_gausse_inside_coutour = gs.removeValueOutsideBorder(E_grd, N_grd, z_int_kr_gausse, polygone)

##Visualiation krigeage : lignes de niveau

gs.plot_contour_2d(E_grd, N_grd, z_int_kr_gausse_inside_coutour, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'krigeage gausse')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

# Visualiation splines de lissage : surface colorée
gs.plot_surface_2d(E_grd, N_grd, z_int_kr_gausse_inside_coutour, E_obs, N_obs, minmax = [17,40], xlabel = 'E [km]', ylabel = 'N [km]', title = 'krigeage gausse' ,zlabel = 'T [°C]' )
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

##

z_int_kr_lin, new_gamma, sigma_int_kr = gs.interp_krg_lin(E_obs, N_obs, T_obs, E_grd, N_grd, list_dist_moy, list_gamma)

## Interpolation en un point de l'espace
zi = gs.interp_krg_lin(E_obs, N_obs, T_obs, np.array([[147]]),  np.array([[6837]]), list_dist_moy, list_gamma)[0]
print("La valeur interpolée à Brest est "+str(zi))
zi = gs.interp_krg_lin(E_obs, N_obs, T_obs, np.array([[669]]),  np.array([[6860]]), list_dist_moy, list_gamma)[0]
print("La valeur interpolée à Champs-sur-Marne est "+str(zi))
zi = gs.interp_krg_lin(E_obs, N_obs, T_obs, np.array([[917]]),  np.array([[6327]]), list_dist_moy, list_gamma)[0]
print("La valeur interpolée à Forcalquier est "+str(zi))


##afficahge nueé et variographe
plt.figure()

plt.scatter(dist, delta)

plt.plot(list_dist_moy, np.array(new_gamma), color="orange")

plt.scatter(list_dist_moy, list_gamma, color="red")

plt.show()

## retrait des zones interpolées en dehors du territoire
z_int_kr_lin_inside_coutour = gs.removeValueOutsideBorder(E_grd, N_grd, z_int_kr_lin, polygone)
sigma_int_kr_lin_inside_coutour = gs.removeValueOutsideBorder(E_grd, N_grd, sigma_int_kr, polygone)

##Visualiation krigeage : lignes de niveau
gs.plot_contour_2d(E_grd, N_grd, z_int_kr_lin, E_obs, N_obs,  xlabel = 'E [km]', ylabel = 'N [km]', title = 'krigeage linéaire')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

# Visualiation splines de lissage : surface colorée
gs.plot_surface_2d(E_grd, N_grd, z_int_kr_lin, E_obs, N_obs, minmax = [17, 40], xlabel = 'E [km]', ylabel = 'N [km]', title = 'krigeage linéaire' ,zlabel = 'T [°C]' )
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

##ecart
gs.plot_contour_2d(E_grd, N_grd, sigma_int_kr, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'krigeage linéaire')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()

# Visualiation splines de lissage : surface colorée
gs.plot_surface_2d(E_grd, N_grd, sigma_int_kr, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'sigma krigeage linéaire' ,zlabel = 'ecartype [°C]' )
plt.plot(contour[:,0],contour[:,1],'k',lw=2)
plt.show()


############################# validation croisée:

##echantillonnage (des points de controle à partir des observations)
pourcentage_echantillon = 0.3 #86
indice_obs_to_delete = np.random.choice(np.arange(E_obs.shape[0]), int(E_obs.shape[0] * pourcentage_echantillon), replace=False) #

echantillon_E_obs = np.take(E_obs, indice_obs_to_delete).reshape(-1, 1)
echantillon_N_obs = np.take(N_obs, indice_obs_to_delete).reshape(-1, 1)
echantillon_T_obs = np.take(T_obs, indice_obs_to_delete).reshape(-1, 1)

##les coordonnées et valeurs obersées des points de controle

E_obs_control_points = np.take(E_obs, indice_obs_to_delete).reshape(-1, 1)
N_obs_control_points = np.take(N_obs, indice_obs_to_delete).reshape(-1, 1)
T_obs_control_points = np.take(T_obs, indice_obs_to_delete).reshape(-1, 1)

##calcul des valeurs d'importpolation pour chaque points de controle
list_erreur_int_lin = []
list_erreur_int_inv4 = []
list_erreur_int_spl103 = []
list_erreur_int_kr = []

list_z_chap_int_lin = []
list_z_chap_int_inv4 = []
list_z_chap_int_spl103 = []
list_z_chap_int_kr  = []

for k in indice_obs_to_delete:

    echantillon_E_obs = np.delete(E_obs, k).reshape(-1, 1)
    echantillon_N_obs = np.delete(N_obs, k).reshape(-1, 1)
    echantillon_T_obs = np.delete(T_obs, k).reshape(-1, 1)

    E_obs_control_point = E_obs[k:k + 1, 0: 1]
    N_obs_control_point = N_obs[k:k + 1, 0: 1]
    T_obs_control_point = T_obs[k:k + 1, 0: 1]

    T_int_lin = gs.interp_lin(echantillon_E_obs, echantillon_N_obs, echantillon_T_obs, E_obs_control_point, N_obs_control_point)
    list_z_chap_int_lin.append(float(T_int_lin))

    T_int_inv4 = gs.interp_inv(echantillon_E_obs, echantillon_N_obs, echantillon_T_obs, E_obs_control_point, N_obs_control_point, 4)
    list_z_chap_int_inv4.append(float(T_int_inv4))

    T_int_spl103 = gs.interp_spl(echantillon_E_obs, echantillon_N_obs, echantillon_T_obs, E_obs_control_point, N_obs_control_point, 10**3)
    list_z_chap_int_spl103.append(float(T_int_spl103))

    dist, delta = gs.nuee(E_obs, N_obs, T_obs)
    (list_dist_moy, list_gamma) = gs.vario(dist, 500, delta)
    T_int_kr = gs.interp_krg_lin(echantillon_E_obs, echantillon_N_obs, echantillon_T_obs, E_obs_control_point, N_obs_control_point, list_dist_moy, list_gamma,)[0]
    list_z_chap_int_kr.append(float(T_int_kr))

    erreur_int_lin = T_int_lin - T_obs_control_point

    erreur_int_inv4   = T_int_lin - T_obs_control_point

    erreur_int_spl103   = T_int_spl103 - T_obs_control_point

    erreur_int_z_int_kr   = T_int_kr - T_obs_control_point

    list_erreur_int_lin.append(float(erreur_int_lin))
    list_erreur_int_inv4.append(float(erreur_int_inv4))
    list_erreur_int_spl103.append(float(erreur_int_spl103))
    list_erreur_int_kr.append(float(erreur_int_z_int_kr))

## stat des valeurs
stat_from_array = np.array([list_erreur_int_lin, list_erreur_int_inv4, list_erreur_int_spl103, list_erreur_int_kr])

means = np.nanmean(stat_from_array, 1)

std = np.nanstd(stat_from_array, 1)

mean_square = np.sqrt(np.nanmean(stat_from_array**2, 1))

etendue = np.nanmax(stat_from_array, 1) - np.nanmin(stat_from_array, 1)

T_obs_control_points_4 = np.hstack((T_obs_control_points, T_obs_control_points, T_obs_control_points, T_obs_control_points))
T_obs_control_points_4 = T_obs_control_points_4.reshape(-1, T_obs_control_points_4.shape[0])

T_int_points_4 = np.hstack((np.array(list_z_chap_int_lin).reshape(-1, 1), np.array(list_z_chap_int_inv4).reshape(-1, 1), np.array(list_z_chap_int_spl103).reshape(-1, 1), np.array(list_z_chap_int_kr).reshape(-1, 1)))
T_int_points_4 = T_int_points_4.reshape(-1, T_int_points_4.shape[0])

z_bar = np.nanmean(T_obs_control_points_4.T, 1).reshape(-1, 1)
z_bar = np.hstack((z_bar, z_bar, z_bar, z_bar)).T


##
willmot = 1 - (np.nansum(stat_from_array**2, 1)/np.nansum( ((np.abs(T_int_points_4 - z_bar)) + np.abs(T_obs_control_points_4 - z_bar))**2, 1))


##
def histo(list_erreur_int, title,  n_bins = 25):
    """
    source:https://matplotlib.org/stable/gallery/statistics/hist.html

    """
    from matplotlib import colors
    from matplotlib.ticker import PercentFormatter

    plt.title(title)

    fig, axs = plt.subplots(1, 2, tight_layout=True)

    # N is the count in each bin, bins is the lower-limit of the bin
    N, bins, patches = axs[0].hist(list_erreur_int, bins=n_bins)

    # We'll color code by height, but you could use any scalar
    fracs = N / N.max()

    # we need to normalize the data to 0..1 for the full range of the colormap
    norm = colors.Normalize(fracs.min(), fracs.max())

    # Now, we'll loop through our objects and set the color of each accordingly
    for thisfrac, thispatch in zip(fracs, patches):
        color = plt.cm.viridis(norm(thisfrac))
        thispatch.set_facecolor(color)

    # We can also normalize our inputs by the total number of counts
    axs[1].hist(list_erreur_int, bins=n_bins, density=True)

    # Now we format the y-axis to display percentage
    axs[1].yaxis.set_major_formatter(PercentFormatter(xmax=1))

    axs[0].set_title(title, loc='center', wrap=True)

    axs[1].set_title(title, loc='center', wrap=True)

    plt.show()


## histogramme
histo(list_erreur_int_lin, "histogramme des erreur de l'interpolation linéaire")
histo(list_erreur_int_inv4, "histogramme des erreur de l'interpolation par inverse des distances ")
histo(list_erreur_int_spl103, "histogramme des erreur de Splines de lissage")
histo(list_erreur_int_kr, "histogramme des erreur de l'interpolation par krigeage ")
## diagramme en bar
def barplot(x_name, y, title):
    plt.figure()

    x_pos = np.arange(len(x_name))

    plt.bar(x_pos, y, color=[ 'red', 'green', 'blue', 'cyan'])

    plt.xticks(x_pos, x_name,  wrap=True)

    plt.title(title)

    plt.show()
##
x = ["interpolation linéaire", "inverse des distances", "Splines de lissage", "krigeage"]

barplot(x, means, "moyenne")
barplot(x, std, "ecart-type")
barplot(x, mean_square, "moyenne quadratique")
barplot(x, etendue, "étendue")
barplot(x, willmot, "willmot")

##
barplot(x, np.abs(means), "valeur absolue des moyennes")









