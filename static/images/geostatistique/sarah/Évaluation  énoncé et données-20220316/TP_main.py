#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##########################################################################
#    TP - Introduction à l'interpolation spatiale et aux géostatistiques #
##########################################################################

# Tiazara Geatan / Humbert Sarah
# Version du 16/03/2022


# Numpy
import numpy as np
# Matplotlib / plot
import matplotlib.pyplot as plt
import math as m
# lib geostatistic
import lib_gs as gs

data = np.loadtxt('FR_T2M_20190629_120000.txt')
E_obs = data[:,0].reshape((-1, 1)) / 1000
N_obs = data[:,1].reshape((-1, 1)) / 1000
T_obs = data[:,2].reshape((-1, 1))


# points à interpoler
# Brest 147 6837
# Champs-sur-Marne 669 6860
# Forcalquier 917 6327



# Visualisation des données en entrée : sites de mesure
#gs.plot_points(E_obs, N_obs, xlabel = 'x [m]', ylabel = 'y [m]', title = "sites d'observation")

# Visualisation des données en entrée : sites de mesure coloré en fonction de la VR
#gs.plot_patch(E_obs, N_obs, T_obs, xlabel = 'x [m]', ylabel = 'y [m]', zlabel = 'z [m]', title = "observations")

# Création d'une grille planimétrique pour l'interpolation
E_grd, N_grd = np.meshgrid(np.linspace(np.floor(np.min(E_obs)), np.ceil(np.max(E_obs)), 100), np.linspace(np.floor(np.min(N_obs)), np.ceil(np.max(N_obs)), 100))



# Interpolation linéaire
z_interpolation_lineaire = gs.interp_lin(E_obs, N_obs, T_obs, E_grd, N_grd)

# Visualiation de l'interpolation linéaire : lignes de niveau
#gs.plot_contour_2d(E_grd, N_grd, z_interpolation_lineaire, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'interpolation linéaire')

# Visualiation de l'interpolation linéaire : surface colorée
#gs.plot_surface_2d(E_grd, N_grd, z_interpolation_lineaire, E_obs, N_obs, minmax = [17,40], xlabel = 'E [km]', ylabel = 'N [km]', title = 'interpolation linéaire')

# Interpolation en un point de l'espace
zi = gs.interp_lin(E_obs, N_obs, T_obs, np.array([[147]]),  np.array([[6837]]))
#print("La valeur interpolée à Brest est "+str(zi))
zi = gs.interp_lin(E_obs, N_obs, T_obs, np.array([[669]]),  np.array([[6860]]))
#print("La valeur interpolée à Champs-sur-Marne est "+str(zi))
zi = gs.interp_lin(E_obs, N_obs, T_obs, np.array([[917]]),  np.array([[6327]]))
#print("La valeur interpolée à Forcalquier est "+str(zi))





# Interpolation inverse des distances
z_inv_2 = gs.interp_inv(E_obs, N_obs, T_obs, E_grd, N_grd, 4)

# Visualiation de l'interpolation linéaire : lignes de niveau
#gs.plot_contour_2d(E_grd, N_grd, z_inv_2, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'inverse des distances')

# Visualiation de l'interpolation linéaire : surface colorée
#gs.plot_surface_2d(E_grd, N_grd, z_inv_2, E_obs, N_obs, minmax = [17,40], xlabel = 'E [km]', ylabel = 'N [km]', title = 'inverse des distances')

# Interpolation en un point de l'espace
zi = gs.interp_inv(E_obs, N_obs, T_obs, np.array([[147]]),  np.array([[6837]]), 4)
#print("La valeur interpolée à Brest est "+str(zi))
zi = gs.interp_inv(E_obs, N_obs, T_obs, np.array([[669]]),  np.array([[6860]]), 4)
#print("La valeur interpolée à Champs-sur-Marne est "+str(zi))
zi = gs.interp_inv(E_obs, N_obs, T_obs, np.array([[917]]),  np.array([[6327]]), 4)
#print("La valeur interpolée à Forcalquier est "+str(zi))



# Interpolation splines
z_spl = gs.interp_spl(E_obs, N_obs, T_obs, E_grd, N_grd, 1000000000000)

# Visualiation splines de lissage : lignes de niveau
#gs.plot_contour_2d(E_grd, N_grd, z_spl, E_obs, N_obs, xlabel = 'E [km]', ylabel = 'N [km]', title = 'splines de lissage')

# Visualiation splines de lissage : surface colorée
#gs.plot_surface_2d(E_grd, N_grd, z_spl, E_obs, N_obs, minmax = [17,40], xlabel = 'E [km]', ylabel = 'N [km]', title = 'splines de lissage')

# Interpolation en un point de l'espace
zi = gs.interp_spl(E_obs, N_obs, T_obs, np.array([[147]]),  np.array([[6837]]), 1000000)
print("La valeur interpolée à Brest est "+str(zi))
zi = gs.interp_spl(E_obs, N_obs, T_obs, np.array([[669]]),  np.array([[6860]]), 1000000)
print("La valeur interpolée à Champs-sur-Marne est "+str(zi))
zi = gs.interp_spl(E_obs, N_obs, T_obs, np.array([[917]]),  np.array([[6327]]), 1000000)
print("La valeur interpolée à Forcalquier est "+str(zi))





contour = np.loadtxt('FR_contour.txt')
plt.plot(contour[:,0],contour[:,1],'k',lw=2)

plt.show()

