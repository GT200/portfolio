#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##########################################################################
#    TP - Introduction à l'interpolation spatiale et aux géostatistiques #
##########################################################################

# P. Bosser / ENSTA Bretagne
# Version du 13/03/2022


# Numpy
import numpy as np
# Matplotlib / plot
import matplotlib.pyplot as plt

################## Modèle de fonction d'interpolation ##################

def interp_xxx(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation par ???
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    z_int = np.nan*np.zeros(x_int.shape)
    #
    # ...
    #
    return z_int

####################### Fonctions d'interpolation ######################

def interp_lin(x_obs, y_obs, z_obs, x_int, y_int):
    from scipy.spatial import Delaunay as delaunay
    # Calcul de la triangulation à partir de coordonnées (x,y)
    tri = delaunay( np.hstack( (x_obs, y_obs) ) )
    # Recherche de l’index du triangle contenant le point x0, y0
    
    z_int_lin = np.nan*np.zeros(x_int.shape)
     
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,y_int.shape[1]):
            # Recherche des index des sommets du triangle contenant le point x0, y0
            
            idx_t = tri.find_simplex( np.array([x_int[i,j], y_int[i,j]]) )
            
            if (idx_t != - 1):
            
                idx_s = tri.simplices[idx_t,:]
        
                
                # Coordonnées des sommets du triangle contenant le point x0, y0
                x1 = float(x_obs[ idx_s[0] ]) ; y1 = float(y_obs[ idx_s[0] ])
                x2 = float(x_obs[ idx_s[1] ]) ; y2 = float(y_obs[ idx_s[1] ])
                x3 = float(x_obs[ idx_s[2] ]) ; y3 = float(y_obs[ idx_s[2] ])
                
                z1 = z_obs[ idx_s[0] ]
                z2 = z_obs[ idx_s[1] ] 
                z3 = z_obs[ idx_s[2] ]
                
                B = np.array([[z1, z2, z3]]).T
                
                A = np.array([[x1, y1, 1], [x2, y2, 1], [x3, y3, 1]])
                
                a,b,g = np.linalg.solve(A, B)[0, 0], np.linalg.solve(A, B)[0, 1], np.linalg.solve(A, B)[0, 2]
                
                z_int_lin[i, j] = a * x_int[i,j] + b * y_int[i,j] +g
    return(z_int_lin)
    
def interp_ppv(x_obs, y_obs, z_obs, x_int, y_int):
    # Interpolation par plus proche voisin
    # x_obs, y_obs, z_obs : observations
    # [np.array dimension 1*n]
    # x_int, y_int, positions pour lesquelles on souhaite interpoler une valeur z_int
    # [np array dimension m*p]
    
    z_int = np.nan*np.zeros(x_int.shape)
    for i in np.arange(0,x_int.shape[0]):
        for j in np.arange(0,x_int.shape[1]):
            z_int[i,j] = z_obs[np.argmin(np.sqrt((x_int[i,j]-x_obs)**2+(y_int[i,j]-y_obs)**2))]
    return z_int
    
def interp_inv_dist(x_obs, y_obs, z_obs, x_grd, y_grd, p):
    
    z_int_inv_dist = np.nan*np.zeros(x_grd.shape)
    
    for i in np.arange(0,x_grd.shape[0]):
        for j in np.arange(0,y_grd.shape[1]):
            
            val_x_grid = np.ones(x_obs.shape) * x_grd[i,j]
            
            val_y_grid = np.ones(x_obs.shape) * y_grd[i,j]
            
            dist = np.sqrt((val_x_grid-x_obs)**2+(val_y_grid-y_obs)**2)
            
            sum1 = np.sum(z_obs/(dist)**p)
            
            sum2 = np.sum(1/(dist)**p)
                
            z_int_inv_dist[i, j] =sum1/sum2
    return(z_int_inv_dist)
    
def interp_spl(x_obs, y_obs, z_obs, x_grd, y_grd, rho):
    z_int_spl = np.nan*np.zeros(x_grd.shape)
    
    def phi(dist):
        
        return((dist**2)*(np.log(dist)))
    
    #rho = 0
    
    B = np.zeros((x_obs.shape[0] + 3, 1))
    
    B[:B.shape[0] - 3, 0:1] = z_obs
    
    A = np.zeros((x_obs.shape[0] + 3, x_obs.shape[0] + 3))
                    
    A[:A.shape[0] - 3, 0] = 1
    
    A[:A.shape[0] - 3, 1] = np.squeeze(x_obs)
    
    A[:A.shape[0] - 3, 2]:3 = np.squeeze(y_obs)
    
    A[A.shape[0] - 3:, 3:] = A[: A.shape[0] - 3:, 0:3].T
    
    A_temp = A[:A.shape[0] - 3, 3:]
    for i in np.arange(0,A_temp.shape[0]):
        for j in np.arange(0,A_temp.shape[1]):
            
            if (i == j):
                A_temp[i,j] = rho
            else:
                si_x = x_obs[i]
                si_y = y_obs[i]
                sj_x = x_obs[j]
                sj_y = y_obs[j]
                dist = np.sqrt((si_x-sj_x)**2+(si_y-sj_y)**2)
                
                phi_dist = phi(dist)
                
                A_temp[i,j] = phi_dist
                
    A[:A.shape[0] - 3, 3:] = A_temp
    
    
    coeff = np.linalg.solve(A, B)
    
    for i in np.arange(0,x_grd.shape[0]):
        for j in np.arange(0,y_grd.shape[1]):
            
            x_s = x_grd[i, j]
            
            y_s = y_grd[i,j]
            
            dist_s_si = np.sqrt((x_obs-x_s)**2+(y_obs-y_s)**2)
            
            
            phi_dist_s_si = phi(dist_s_si)
            
            bi = coeff[3:, 0:1]
            
            sumphi = np.sum(bi * phi_dist_s_si)
            
            sum_ai = float(np.array([[1, x_s, y_s]])@coeff[:3, 0:1])
            
            z_int_spl[i, j] = sumphi + sum_ai
    
    return(z_int_spl)
    
def nuee(x_obs, y_obs, z_obs):
    dist = []
    delta = []
    

    for i in np.arange(0,x_obs.shape[0]):
        for j in np.arange(0,y_obs.shape[0]):
            
            if (i!=j): 
                si_x = x_obs[i]
                si_y = y_obs[i]
                
                sj_x = x_obs[j]
                sj_y = y_obs[j]
                
                zi = z_obs[i]
                zj = z_obs[j]
                
                d = np.sqrt((si_x-sj_x)**2+(si_y-sj_y)**2)
                
                dist.append(d)
                
                delta.append((1/2) * (zi - zj)**2)

    return(np.array(dist), np.array(delta))
    
def vario(dist, pas, delta):
    
    indice_dist_sort = np.argsort(np.squeeze(dist))
    
    dist_sort = np.take(dist, indice_dist_sort)
    
    delta_sort = np.take(delta, indice_dist_sort)
    
    print(delta_sort)
    
    #print(delta_sort)
    
    list_moy = []
    
    list_gamma = []
    
    n = dist_sort.shape[0]
    
    print(n)
    
    for i in range(0, dist_sort.shape[0], pas):
        #print(i)
        if ((i + pas) <n):
            dist_moy = np.mean(dist_sort[i: i + pas])
            
            gamma = np.sum(delta_sort[i: i + pas])/pas
            
            #print(gamma)
            
            list_moy.append(dist_moy)
            
            list_gamma.append(gamma)
            
    return(np.array(list_moy), np.array(list_gamma))


def interp_krg(x_obs, y_obs, z_obs, x_grd, y_grd, c0,a0, list_dist_moy, list_gamma, epislone):
    
    def dha(a0, c0, hi):
        return c0 * ( (-14 * hi**2 * a0**(-3)) + ( (105/4) * (hi**3) * a0**(-4)) - ( (35/2) * (hi**5) * a0**(-6)) + ( (21/4) * (hi**7) * a0**(-8)) )  
        
    def dhc(a0, hi):
        return ( (7 * hi**2 * a0**(-2)) - ( (35/4) * (hi**3) * a0**(-3))  +  ( (7/2) * hi**5 * a0**(-5)) -  ( (3/4) * hi**7 * a0**(-7)) )
    
    def gamma(hi, a0, c0):
        
        return c0 * ( (7 * (hi**2) * a0**(-2)) - ( (35/4) * (hi**3) * a0**(-3))  +  ( (7/2) * hi**5 * a0**(-5)) -  ( (3/4) * (hi**7) * a0**(-7)) )
        
    
    while True:
        
        A = np.zeros((list_dist_moy.shape[0], 2))
        
        B = np.zeros((list_dist_moy.shape[0], 1))
        
        for i in range(list_dist_moy.shape[0]):
            
            hi = list_dist_moy[i]
            
            if (hi > a0):
                
                A[i, 0] = 1
                
                A[i, 1] = 0 
                
                B[i, 0] = list_gamma[i] - c0
                
                
            else:
            
                A[i, 0] = dhc(a0, hi)
                
                A[i, 1] = dha(a0, c0, hi)
                
                B[i, 0] = list_gamma[i] - gamma(hi, a0, c0)
                
            
        X = np.linalg.lstsq(A, B)
        
        (dc, da) = X[0]
        
        print(X[0])
    

        
        if ((dc < epislone) or (da < epislone)):
            break
        else:
            c0 += dc 
        
            a0 += da
            
    
    new_gamma = []
        
    for k in range(list_gamma.shape[0]):
        new_gamma.append(float(gamma(list_dist_moy[k], a0, c0)))
        

        
    
    ########## kr 
    
    A = np.zeros((x_obs.shape[0] + 1, y_obs.shape[0] + 1))
    
    z_int_kr = np.nan*np.zeros(x_grd.shape)
    
    for i in np.arange(0,x_obs.shape[0]):
        for j in np.arange(0,y_obs.shape[0]):
            
            if ((i!=j) and (i != A.shape[0] - 1) and (i != A.shape[1] - 1)):
                si_x = x_obs[i]
                si_y = y_obs[i]
                
                sj_x = x_obs[j]
                sj_y = y_obs[j]
                
                zi = z_obs[i]
                zj = z_obs[j]
                
                d = np.sqrt((si_x-sj_x)**2+(si_y-sj_y)**2)
                
                gam = gamma(d, a0, c0)
                
                A[i, j] = gam
                
    A[A.shape[0] - 1, : A.shape[1] - 1] = 1
    
    A[:A.shape[0] - 1, A.shape[1] - 1] = 1
    
    for i in np.arange(0,x_grd.shape[0]):
        for j in np.arange(0,y_grd.shape[1]):
            B = np.ones((x_obs.shape[0] + 1, 1))
            s_x = x_grd[i, j]
            s_y = y_grd[i, j]
            
            d = np.sqrt((x_obs-s_x)**2+(y_obs-s_y)**2)
            
            gam = gamma(d, a0, c0)
            
            B[:B.shape[0] - 1, 0:1] = gam
            
            coeff = np.linalg.solve(A, B)
            
            z_int_kr[i, j] = coeff[:coeff.shape[0] - 1, 0]@z_obs
            
    return (z_int_kr, new_gamma)
            

############################# Visualisation ############################
def showScatter(dist, delta):
    plt.figure()
    plt.scatter(dist, delta)
    plt.show()

def plot_contour_2d(x_grd ,y_grd ,z_grd, x_obs = np.array([]) ,y_obs = np.array([]), xlabel = "", ylabel = "", title = "", fileo = ""):
    # Tracé du champ interpolé sous forme d'isolignes
    # x_grd, y_grd, z_grd : grille de valeurs interpolées
    # x_obs, y_obs : observations (facultatif)
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    
    z_grd_m = np.ma.masked_invalid(z_grd)
    fig = plt.figure()
    plt.contour(x_grd, y_grd, z_grd_m, int(np.round((np.max(z_grd_m)-np.min(z_grd_m))/4)),colors ='k')
    if x_obs.shape[0]>0:
        plt.scatter(x_obs, y_obs, marker = 'o', c = 'k', s = 5)
        plt.xlim(0.95*np.min(x_obs),np.max(x_obs)+0.05*np.min(x_obs))
        plt.ylim(0.95*np.min(y_obs),np.max(y_obs)+0.05*np.min(y_obs))
    else:
        plt.xlim(0.95*np.min(x_grd),np.max(x_grd)+0.05*np.min(x_grd))
        plt.ylim(0.95*np.min(y_grd),np.max(y_grd)+0.05*np.min(y_grd))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_surface_2d(x_grd ,y_grd ,z_grd, x_obs = np.array([]) ,y_obs = np.array([]), minmax = [0,0], xlabel = "", ylabel = "", zlabel = "", title = "", fileo = ""):
    # Tracé du champ interpolé sous forme d'une surface colorée
    # x_grd, y_grd, z_grd : grille de valeurs interpolées
    # x_obs, y_obs : observations (facultatif)
    # minmax : valeurs min et max de la variable interpolée (facultatif)
    # xlabel, ylabel, zlabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    
    from matplotlib import cm
    
    z_grd_m = np.ma.masked_invalid(z_grd)
    fig = plt.figure()
    if minmax[0] < minmax[-1]:
        p=plt.pcolormesh(x_grd, y_grd, z_grd_m, cmap=cm.terrain, vmin = minmax[0], vmax = minmax[-1], shading = 'auto')
    else:
        p=plt.pcolormesh(x_grd, y_grd, z_grd_m, cmap=cm.terrain, shading = 'auto')
    if x_obs.shape[0]>0:
        plt.scatter(x_obs, y_obs, marker = 'o', c = 'k', s = 5)
        plt.xlim(0.95*np.min(x_obs),np.max(x_obs)+0.05*np.min(x_obs))
        plt.ylim(0.95*np.min(y_obs),np.max(y_obs)+0.05*np.min(y_obs))
    else:
        plt.xlim(0.95*np.min(x_grd),np.max(x_grd)+0.05*np.min(x_grd))
        plt.ylim(0.95*np.min(y_grd),np.max(y_grd)+0.05*np.min(y_grd))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    fig.colorbar(p,ax=plt.gca(),label=zlabel,fraction=0.046, pad=0.04)
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_points(x_obs, y_obs, xlabel = "", ylabel = "", title = "", fileo = ""):
    # Tracé des sites d'observations
    # x_obs, y_obs : observations
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    
    fig = plt.figure()
    ax = plt.gca()
    plt.plot(x_obs, y_obs, 'ok', ms = 4)
    ax.set_xlim(0.95*min(x_obs),max(x_obs)+0.05*min(x_obs))
    ax.set_ylim(0.95*min(y_obs),max(y_obs)+0.05*min(y_obs))
    ax.set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_patch(x_obs, y_obs, z_obs, xlabel = "", ylabel = "", zlabel = "", title = "", fileo = ""):
    # Tracé des valeurs observées
    # x_obs, y_obs, z_obs : observations
    # xlabel, ylabel, zlabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    
    from matplotlib import cm
    
    fig = plt.figure()
    p=plt.scatter(x_obs, y_obs, marker = 'o', c = z_obs, s = 80, cmap=cm.terrain)
    plt.xlim(0.95*min(x_obs),max(x_obs)+0.05*min(x_obs))
    plt.ylim(0.95*min(y_obs),max(y_obs)+0.05*min(y_obs))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    fig.colorbar(p,ax=plt.gca(),label=zlabel,fraction=0.046, pad=0.04)
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')

def plot_triangulation(x_obs, y_obs, xlabel = "", ylabel = "", title = "", fileo = ""):
    # Tracé de la triangulation sur des sites d'observations
    # x_obs, y_obs : observations
    # xlabel, ylabel : étiquettes des axes (facultatif)
    # title : titre (facultatif)
    # fileo : nom du fichier d'enregistrement de la figure (facultatif)
    from scipy.spatial import Delaunay as delaunay
    tri = delaunay(np.hstack((x_obs,y_obs)))
    
    plt.figure()
    plt.triplot(x_obs[:,0], y_obs[:,0], tri.simplices)
    plt.plot(x_obs, y_obs, 'or', ms=4)
    plt.xlim(0.95*min(x_obs),max(x_obs)+0.05*min(x_obs))
    plt.ylim(0.95*min(y_obs),max(y_obs)+0.05*min(y_obs))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid()
    if not fileo == "": plt.savefig(fileo,bbox_inches='tight')
