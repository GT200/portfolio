#from etape_1.affichage import AffichageCarte
from etape_1.distance_spherique import DistanceSpheriqueMinMax
from etape_1.presence_en_jours import BilanPresence
from etape_1.creer_fichier_core import ExtraireCore
from etape_1.dbks2 import DBKS

from etape_2.temperature_initiale import SelectSousReseauMatrice
from etape_2.normalisation import NormalisationDeCritere
from etape_2.temperature_initiale import TemperatureInitiale
from etape_2.affichage_critere import AfficheCritere

from etape_3.recuit_simule import RecuitSimule

from etape_3_bis.temperature_initiale_v2 import TemperatureInitialeV2
from etape_3_bis.recuit_simule_v2 import RecuitSimuleV2
from etape_3_bis.recuit_simule_v2 import ExtractTrace

from etape_3_bis.choisir_station_core import FichierCore20

import numpy as np

#L'affichage a été retiré car repl ne possède pas les biblio nécessaires
#et donc, dans le main, il sera mis en commentaire
if __name__ == "__main__":
    # -- extraction des donnees igs pour les 124 stations -- #
    data_igs_124 = np.genfromtxt("Donnee_source/data_IGS_10466.dat")
    (n, p) = data_igs_124.shape
    longitudes_deg_igs_124 = data_igs_124[:, 1:2]
    latitudes_deg_igs_124 = data_igs_124[:, 2:3]
    jours_de_pressence_igs_124 = data_igs_124[:, 3:4]
    print("*" * 20)

    # Réponse à la question 1
    # -- affichage de la carte localisant les stations -- #
    #AffichageCarte(longitudes_deg_igs_124, latitudes_deg_igs_124, "IGS NETWORK (124 STATIONS)")
    print("Affichage map_data_igs_124")
    print("*" * 20)

    # -- calcul de distance sphérique maximal et minimal -- #
    precision = 3
    (ds_min, ds_max) = DistanceSpheriqueMinMax(longitudes_deg_igs_124,
                                               latitudes_deg_igs_124,
                                               precision)
    print("distance sphérique minimale:", ds_min, "km")
    print("distance sphérique maximale:", ds_max, "km")
    print("*" * 20)

    # -- bilan statistique sur les jours de présences -- #
    presence_min, presence_moyenne, presence_max = BilanPresence(
        jours_de_pressence_igs_124)
    print("présence minimale:", presence_min, "jours")
    print("présence moyenne:", presence_moyenne, "jours")
    print("présence maximale:", presence_max, "jours")
    print("*" * 20)

    # -- indicateur DBKS -- #
    dbks_124 = DBKS(longitudes_deg_igs_124, latitudes_deg_igs_124, precision)
    print("dbks_124:", dbks_124)
    print("*" * 20)

    # Réponse à la question 2
    # -- creation du fichier core -- #
    ExtraireCore("Donnee_source/data_IGS_10466.dat",
                 "Donnee_source/IGb14_core.dat",
                 "Donnee_creee/data_IGS_10466_core.dat")

    # -- extraction des donnes igs core pour les 43 stations -- #
    data_igs_14_core = np.genfromtxt("Donnee_creee/data_IGS_10466_core.dat")

    (n_core, p_core) = data_igs_14_core.shape
    #print(data_igs_14_core)

    longitudes_deg_igs_14_core = data_igs_14_core[:, 1:2]
    latitudes_deg_igs_14_core = data_igs_14_core[:, 2:3]
    jours_de_presence_igs_14_core = data_igs_14_core[:, 3:4]
    print("*" * 20)

    # -- affichage de la carte localisant les stations -- #
    print("Affichage map_data_igs_14_core")
    #AffichageCarte(longitudes_deg_igs_14_core, latitudes_deg_igs_14_core, "IGS SUB-NETWORK (CORE {} STATION)".format(n_core))
    print("*" * 20)

    # -- calcul de distance sphérique maximal et minimal -- #
    precision = 3
    (ds_min_core,
     ds_max_core) = DistanceSpheriqueMinMax(longitudes_deg_igs_14_core,
                                            latitudes_deg_igs_14_core,
                                            precision)
    print("distance sphérique minimale:", ds_min_core, "km")
    print("distance sphérique maximale:", ds_max_core, "km")
    print("*" * 20)

    # -- bilan statistique sur les jours de présences -- #
    presence_min_core, presence_moyenne_core, presence_max_core = BilanPresence(
        jours_de_presence_igs_14_core)
    print("présence minimale:", presence_min_core, "jours")
    print("présence moyenne:", presence_moyenne_core, "jours")
    print("présence maximale:", presence_max_core, "jours")
    print("*" * 20)
    # -- indicateur DBKS -- #
    dbks_core = DBKS(longitudes_deg_igs_14_core, latitudes_deg_igs_14_core,
                     precision)
    print("dbks_core:", dbks_core)
    print("*" * 20)

    # Réponse à la question 3: cf rapport

    # Réponse à la question 4 (pour le réseau core)

    # -- normalisation -- #
    print("critères normralisés pour les 43 stations core")
    ds_min_norm = NormalisationDeCritere(ds_max, ds_min, ds_min_core,
                                         precision)
    presence_moyenne_norm = NormalisationDeCritere(presence_max, presence_min,
                                                   presence_moyenne_core,
                                                   precision)
    dbks_norm = NormalisationDeCritere(0, 0.5, dbks_core, precision)
    f_objectif = round(
        (1 / 3) * (ds_min_norm + presence_moyenne_norm + dbks_norm), 3)
    #print(ds_min_norm, presence_moyenne_norm, dbks_norm)
    print("distance spérique normalisée:", ds_min_norm)
    print("jours de présence normalisée:", presence_moyenne_norm)
    print("dbks normalisée:", dbks_norm)
    print("fonction objectif: ", f_objectif)
    print("*" * 20)

    # -- calcul de la temperature initiale -- #
    print("Calcul de T0")
    nb_variation = 5
    #(T0, list_norm_ds, list_norm_presence, list_norm_dbks, list_f_ojectif) = TemperatureInitiale("Donnee_source/data_IGS_10466.dat","Donnee_creee/trace_{}_variation.dat".format(nb_variation), ds_min, ds_max, presence_min, presence_max, nb_variation, 0.8, precision)

    # Temperature intiale retenue pour nb_variation = 1000 des 124 station est de 0.17
    #print("Température initiale",T0)
    print("*" * 20)

    print("affichage des critères pour 124 stations ")
    #AfficheCritere(list_norm_ds, ds_min_norm, 'Graphe distance sphérique de chaque variation aléatoire ({})'.format(n), label_x = "variation", label_y = "norm_ds")
    #AfficheCritere(list_norm_presence, presence_moyenne_norm, 'Graphe des jours de présence de chaque variation aléatoire ({})'.format(n), label_x = "variation", label_y = "norm_presence")
    #AfficheCritere(list_norm_dbks, dbks_norm, 'Graphe des norm_DBKS de chaque variation aléatoire ({})'.format(n), label_x = "variation", label_y = "norm_dbks")
    #AfficheCritere(list_f_ojectif, f_objectif, 'Graphe des fonctions objectifs de chaque variation aléatoire ({})'.format(n), label_x = "variation", label_y = "fonction objectif")
    print("*" * 20)

    print("Recuit simulé pour les 124 station et {} variations".format(
        nb_variation))
    #RecuitSimule("Donnee_source/data_IGS_10466.dat", "Donnee_creee/trace_recuit.dat", ds_min, ds_max, presence_min, presence_max, nb_variation, 0.8, 0.17, 0.995, precision)
    print("*" * 20)

    print("Extraction/affichage des traces recruit 124 station".upper())

    #(l_ds_124, l_pr_124, l_dbks_124, l_f_o_124, station_124)= ExtractTrace("Donnee_creee/trace_iteration_retenue.dat")

    #AfficheCritere(l_f_o_124, f_objectif, 'Graphe des fonctions objectifs de chaque variation aléatoire ({})'.format(n), label_x = "variation", label_y = "fonction objectif")

    print("*" * 20)
    print("Reprise de toutes les étapes avec 20 stations".upper())
    print("*" * 20)
    # -- extraction des donnees igs pour les 20 stations -- #
    data_igs_20 = np.genfromtxt("Donnee_source/data_IGS_10466_20_stations.dat")
    (n, p) = data_igs_20.shape
    longitudes_deg_igs = data_igs_20[:, 1:2]
    latitudes_deg_igs = data_igs_20[:, 2:3]
    jours_de_pressence_igs = data_igs_20[:, 3:4]
    print("*" * 20)

    # Réponse à la question 1
    # -- affichage de la carte localisant les stations -- #
    #AffichageCarte(longitudes_deg_igs, latitudes_deg_igs, "IGS NETWORK (20 STATIONS)")
    print("Affichage map_data_igs_20")
    print("*" * 20)

    # -- calcul de distance sphérique maximal et minimal -- #
    precision = 3
    (ds_min, ds_max) = DistanceSpheriqueMinMax(longitudes_deg_igs,
                                               latitudes_deg_igs, precision)
    print("distance sphérique minimale:", ds_min, "km")
    print("distance sphérique maximale:", ds_max, "km")
    print("*" * 20)

    # -- bilan statistique sur les jours de présences -- #
    presence_min, presence_moyenne, presence_max = BilanPresence(
        jours_de_pressence_igs)
    print("présence minimale:", presence_min, "jours")
    print("présence moyenne:", presence_moyenne, "jours")
    print("présence maximale:", presence_max, "jours")
    print("*" * 20)

    # -- indicateur DBKS -- #
    dbks = DBKS(longitudes_deg_igs, latitudes_deg_igs, precision)
    print("dbks:", dbks)
    print("*" * 20)

    # Réponse à la question 2
    # -- creation du fichier core -- #
    ExtraireCore("Donnee_source/data_IGS_10466_20_stations.dat",
                 "Donnee_source/IGb14_core.dat",
                 "Donnee_creee/data_IGS_10466_20_stations_core.dat")

    # -- extraction des donnes igs core pour les 20 stations -- #
    data_igs_14_core = np.genfromtxt(
        "Donnee_creee/data_IGS_10466_20_stations_core.dat")

    (n_core, p_core) = data_igs_14_core.shape
    #print(data_igs_14_core)

    longitudes_deg_igs_14_core = data_igs_14_core[:, 1:2]
    latitudes_deg_igs_14_core = data_igs_14_core[:, 2:3]
    jours_de_presence_igs_14_core = data_igs_14_core[:, 3:4]
    print("*" * 20)

    # -- affichage de la carte localisant les stations -- #
    #AffichageCarte(longitudes_deg_igs_14_core, latitudes_deg_igs_14_core, "IGS SUB-NETWORK (CORE {} STATIONS)").format(n_core)
    print("Affichage map_data_igs_14_core_20_stations")
    print("*" * 20)

    # -- calcul de distance sphérique maximal et minimal -- #
    precision = 3
    (ds_min_core,
     ds_max_core) = DistanceSpheriqueMinMax(longitudes_deg_igs_14_core,
                                            latitudes_deg_igs_14_core,
                                            precision)
    print("distance sphérique minimale:", ds_min_core, "km")
    print("distance sphérique maximale:", ds_max_core, "km")
    print("*" * 20)

    # -- bilan statistique sur les jours de présences -- #
    presence_min_core, presence_moyenne_core, presence_max_core = BilanPresence(
        jours_de_presence_igs_14_core)
    print("présence minimale:", presence_min_core, "jours")
    print("présence moyenne:", presence_moyenne_core, "jours")
    print("présence maximale:", presence_max_core, "jours")
    print("*" * 20)
    # -- indicateur DBKS -- #
    dbks_core = DBKS(longitudes_deg_igs_14_core, latitudes_deg_igs_14_core,
                     precision)
    print("dbks_core:", dbks_core)
    print("*" * 20)

    # -- normalisation -- #
    print("critères noralisés pour les {} stations core".format(n))
    ds_min_norm = NormalisationDeCritere(ds_max, ds_min, ds_min_core,
                                         precision)
    presence_moyenne_norm = NormalisationDeCritere(presence_max, presence_min,
                                                   presence_moyenne_core,
                                                   precision)
    dbks_norm = NormalisationDeCritere(0, 0.5, dbks_core, precision)
    f_objectif = round(
        (1 / 3) * (ds_min_norm + presence_moyenne_norm + dbks_norm), 3)
    #print(ds_min_norm, presence_moyenne_norm, dbks_norm)
    print("distance spérique normalisée:", ds_min_norm)
    print("jours de présence normalisée:", presence_moyenne_norm)
    print("dbks normalisée:", dbks_norm)
    print("fonction objectif: ", f_objectif)
    print("*" * 20)

    # -- Calcul de la temeperature initiale -- #
    print("Calcul de T0")
    nb_variation = 5
    #(T0, list_norm_ds, list_norm_presence, list_norm_dbks, list_f_ojectif) = TemperatureInitialeV2("Donnee_source/data_IGS_10466_20_stations.dat","Donnee_creee/20_sations_trace_{}_variation.dat".format(nb_variation), ds_min, ds_max, presence_min, presence_max, nb_variation, 0.8, precision)
    #print("Temperature initiale retenue pour nb_variation = 100 des 20 station est de 0.09")
    #print("Température initiale",T0)
    print("*" * 20)

    print("affichage des critères pour 20 station ")
    #AfficheCritere(list_norm_ds, ds_min_norm, nb_variation, 'Graphe distance sphérique de chaque variation aléatoire ({})'.format(n), label_x = "variation", label_y = "norm_ds")
    #AfficheCritere(list_norm_presence, presence_moyenne_norm, 'Graphe des jours de présence de chaque variation aléatoire ({})'.format(n), label_x = "variation", label_y = "norm_presence")
    #AfficheCritere(list_norm_dbks, dbks_norm, 'Graphe des norm_DBKS de chaque variation aléatoire ({})'.format(n), label_x = "variation", label_y = "norm_dbks")
    #AfficheCritere(list_f_ojectif, f_objectif, 'Graphe des fonctions objectifs de chaque variation aléatoire ({})'.format(n), label_x = "variation", label_y = "fonction objectif")
    print("*" * 20)

    print("Recuit simnulé pour les 20 stations et {} variation".format(
        nb_variation))
    #RecuitSimuleV2("Donnee_source/data_IGS_10466_20_stations.dat","Donnee_creee/20_sations_trace_recuit.dat", ds_min, ds_max, presence_min, presence_max, nb_variation, 0.8, 0.09, 0.995, precision)
    print("*" * 20)

    print("Extraction/affichage des traces et du meilleur sous reseau".upper())

    (l_ds_20, l_pr_20, l_dbks_20, l_f_o_20, station_20) = ExtractTrace(
        "Donnee_creee_retenue/20_sations_trace_recuit_retenue.dat")

    #AfficheCritere(l_f_o_20, f_objectif, 'Graphe des fonctions objectifs de chaque variation aléatoire ({})'.format(n), label_x = "variation", label_y = "fonction objectif")

    FichierCore20("Donnee_source/data_IGS_10466_20_stations.dat", l_f_o_20,
                  station_20, "Donnee_creee/core_automatise_20.dat")

    # -- extraction des donnes igs core automatisées pour les 20 stations -- #
    data_core_20 = np.genfromtxt("Donnee_creee/core_automatise_20.dat")

    (n_core, p_core) = data_core_20.shape

    longitudes_deg_igs_14_core = data_core_20[:, 1:2]
    latitudes_deg_igs_14_core = data_core_20[:, 2:3]

    # -- affichage de la carte localisant les stations -- #
    print("Affichage map_data_igs_14_core")
    #AffichageCarte(longitudes_deg_igs_14_core, latitudes_deg_igs_14_core, "IGS SUB-NETWORK (CORE AUTOMATISE {} STATIONS)".format(n_core))
    print("*" * 20)
