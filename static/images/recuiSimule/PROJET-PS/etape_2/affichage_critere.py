# -*- coding: utf-8 -*-

"""
ceated on: wednesday, 27th jumuary 2021

@author: GT

"""
import numpy as np
import matplotlib.pyplot as plt

def AfficheCritere(critere_variation, core_43 , titre, label_x = "X", label_y = "Y"):
    """
    Fonction qui permet d'afficher sous forme graphique les résultats obtenus et stockés dans des fichiers

    :param critere_variation:  list/array de critère pour une variation donnée
    :param type: list/array
    :param core_43: critère pour 43 stations
    :param type: float
    
    """

    fig = plt.figure()
    #f = [core_43] + critere_variation
    f = critere_variation
    plt.scatter(np.arange(1, len(critere_variation) + 1 ), f)
    plt.scatter(0, core_43, color = "red")
    plt.annotate("core_from_clusters", (0, core_43))
    plt.xlabel(label_x)
    plt.ylabel(label_y)
    plt.title(titre)

    plt.show()
