# -*- coding: utf-8 -*-

"""
ceated on: wednesday, 21th jumuary 2021

@author: GT

"""
# à revoir
def NormalisationDeCritere(critere_reseau_1, critere_reseau_2, critere_sous_reseau, precision):
    """
    Fonction qui normalise les critères à optimiser

    :param critere_reseau_1, critere_reseau_2: valeur min/max du réseau global
    :type critere_reseau_1, critere_sous_reseau_2: float
    :param critere_sous_reseau: critere du sous-réseau à normaliser
    :param type: float
    :param precision: nombre de chiffre après la virgule
    :type precision: int
    :return: parmètre nomalisé
    :rtyme: float
    """
    norm = ( critere_reseau_1 - critere_sous_reseau)/(critere_reseau_1 - critere_reseau_2)

    return(round(norm, precision))
