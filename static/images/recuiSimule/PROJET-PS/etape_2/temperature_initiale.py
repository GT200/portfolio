# -*- coding: utf-8 -*-

"""
ceated on: wednesday, 21th jumuary 2021

@author: GT & Juliette

"""
import numpy as np
from math import log

from etape_1.distance_spherique import DistanceSpheriqueMinMax
from etape_1.presence_en_jours import BilanPresence
from etape_1.creer_fichier_core import ExtraireCore
from etape_1.dbks2 import DBKS
from etape_2.normalisation import NormalisationDeCritere

def SelectSousReseauMatrice(matrice_de_donnee):
    """
    Récupère à partir de la matrice_de_donnee à laquelle nous avons ajouté des 0 et des 1 dans une 5e colonne, les stations appartenant au sous-réseau obtenu aléatoirement (où on a 1 à la 5e colonne)

    :param matrice_de_donnee: fichier du reseau sous forme de matrice avec la 5e colonne avec des 0 et des 1
    :param type: np.array
    
    :return sous_reseau_core: matrice où il ne reste que les  stations avec une 5e colonne qui vaut 1
    :rtype: np.array

    """
    (n,p) = matrice_de_donnee.shape
    vecteur_sous_reseau = matrice_de_donnee[ : , p-1:p]
    #print(vecteur_sous_reseau)
    lignes_non_sous_reseau_core = np.where(vecteur_sous_reseau == '0')[0]
    sous_reseau_core = np.delete(matrice_de_donnee, lignes_non_sous_reseau_core, axis = 0)
    
    return(sous_reseau_core)


# fonction adaptée
def Penalites(L):
    """
    Fonction qui attribue des bonus/malus à la fonction objectif selon le nombre de stations du sous-réseau

    :param L: nb de stations du sous-reseau considere
    :param type: int
    :return p: bonus/malus attribue à la fonction objectif selon le nb de stations
    :rtype: float

    """
    p = 0
    if L>50:
        p = (L-50)/45
    elif L<40:
        p = (40-L)/45
    
    return p

def TemperatureInitiale(fichier_igs_14, fichier_sortie, ds_min, ds_max, pr_min, pr_max, nb_variation, taux, precision):
    """
    crée un nombre "nb_variation" possibilité de sous réseau core pour détermier la température initiale du recuit simulé et garde les traces dans fichier_sortie

    :param fichier_igs_14: nom du fichier IGS14 pour le jour considérée
    :type fichier_donnee_igs14: file, extention: dat
    :param fichier_sortie: fichiers contenant la trace du réseau ayant servi à calculer la température initiale
    :type fichier_sortie: file, extention: dat
    :param (ds_min, ds_max, pr_min, pr_max): distance min/max, pésence min/max pour l'ensemble du réseau considéré
    :type (ds_min, ds_max, pr_min, pr_max): float
    :param nb_variation: nombre de variation
    :type nb_variation: int
    :param taux: taux d'acceptation 
    :type taux: float
    :param precision: preciser le nombre de chiffre après la virgule
    :type precision: int
    :return: Température initiale et les critères
    :rtype: tuple

    """
    # Solution initiale aléatoire 
    data = np.genfromtxt(fichier_igs_14, dtype = str)
    (n, p) = data.shape

    vecteur_sous_reseau = np.random.randint(0,2,(n,1)).astype(str)

    data = np.hstack((data, vecteur_sous_reseau))

    # listes et variables utiles
    l_f_objectif = []
    l_f_objectif_degrade = []
    l_ds_morm = []
    l_presense_norm = []
    l_dbks_norm = []
    m1, m2 = 0, 0

    trace = open(fichier_sortie, 'w')

    for variation in range(nb_variation):
        # Extration du sous réseau core (les stations qui lié au chiffre 1 dans le vecteur de sous réseau)
        data_igs_14_core = SelectSousReseauMatrice(data)
        (n_core, p_core) = data_igs_14_core.shape
        longitudes_deg_igs_14_core = data_igs_14_core[:, 1:2].astype(float)
        latitudes_deg_igs_14_core = data_igs_14_core[:, 2:3].astype(float)
        jours_de_presence_igs_14_core = data_igs_14_core[:, 3:4].astype(float)
        # Calcul des critères
        (ds_min_core, ds_max_core) = DistanceSpheriqueMinMax(longitudes_deg_igs_14_core, latitudes_deg_igs_14_core, precision)
        presence_min_core, presence_moyenne_core, presence_max_core = BilanPresence(jours_de_presence_igs_14_core)
        dbks_core = DBKS(longitudes_deg_igs_14_core, latitudes_deg_igs_14_core, precision)

        # normalisation 
        ds_min_norm = NormalisationDeCritere(ds_max, ds_min, ds_min_core, precision)
        presence_moyenne_norm = NormalisationDeCritere(pr_max, pr_min, presence_moyenne_core, precision)
        dbks_norm = NormalisationDeCritere(0, 0.5, dbks_core, precision)

        l_ds_morm.append(ds_min_norm)
        l_presense_norm.append(presence_moyenne_norm)
        l_dbks_norm.append(dbks_norm)
        

        # mise à jour des variables de calcul de la temperature initiale
        f_objectif = (1/3) * (ds_min_norm + presence_moyenne_norm + dbks_norm)
        penalite = Penalites(n_core)
        f_objectif_new= f_objectif + penalite
        if len(l_f_objectif) != 0:
            if f_objectif_new - l_f_objectif[-1] < 0:
                m1 += 1
            else:
                m2 += 1
                l_f_objectif_degrade.append(f_objectif_new)

        l_f_objectif.append(f_objectif_new)
        
        # garder les traces dans un fichier
        sub_net_vector = list(data[:, - 1].astype(int))

        list_trace = [ds_min_norm, presence_moyenne_norm, dbks_norm,round(f_objectif_new, precision), n_core]
        
        trace.write(str(list_trace) + "\n")
        trace.write(str(sub_net_vector) + "\n")
    
        # mis à jour de "data" en passant à une autre variation possible de sous réseau core

        flip = np.random.randint(n)
        
        if data[flip, -1] == "0":
            data[flip, -1] = "1"
        else:
            data[flip, -1] = "0"
        
        print("variation numéro:",variation)
    #calcul de l'ecart moyen de f_objecitf sur les varialtion qui dégrade f_objectif
    n = len(l_f_objectif_degrade)
    m = sum(l_f_objectif_degrade)/n # moyenne des fonction objectif dégradant
    delta_f = sum([ abs(f - m) for f in l_f_objectif_degrade])/n

    # calul de T0 

    T0 = delta_f/log(m2/(m2 * taux - m1 * (1 - taux)))
    
    trace.write(str(list(data[:, 0])) + "\n")
    trace.close()

    return(T0, l_ds_morm, l_presense_norm, l_dbks_norm, l_f_objectif)
