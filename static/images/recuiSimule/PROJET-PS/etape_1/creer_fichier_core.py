# -*- coding: utf-8 -*-
"""
created on sunday, 17th january 2021

@author: GT

"""


def ExtraireCore(fichier_donnee_igs14, fichier_donnee_cluster_igs14, fichier_sortie = "data_IGS_10466_core.dat"):
    """
    crée un fichier pour le sous réseau core de IGS14 en utilisant le fichier data_IGS_10466.dat et IGb14_core.dat. On ne prend une station de data_IGS_10466.dat seulement s'il est aussi présent dans IGb14_core.dat.  L'organisation du nouveau fichier sera similaire à l'organisation du fichier data_IGS_10466.dat

    :param fichier_donnee_igs14: nom du fichier IGS14 pour le jour considérée
    :type fichier_donnee_igs14: file, extention: dat
    :param fichier_donnee_cluster_igs14: nom du fichier IGS14_core contenant les clusters
    :type fichier_donnee_cluster_igs14: file, extention: dat
    :param fichier_sortie: nom du nouveau fichier 
    :type fichier_sortie: file, extention: dat
    """
    # Ouverture des fichiers
    try:
        with open(fichier_donnee_igs14, 'r') as igs14_data:
            lines_igs14_data = igs14_data.readlines()
    except IOError:
        raise IOError("Le fichier {} n'a pas été correctement ouvert".format(fichier_donnee_igs14))

    try:
        with open(fichier_donnee_cluster_igs14, 'r') as cluster_igs14_core_data:
            lines_cluster_igs14_core_data = cluster_igs14_core_data.readlines()
    except IOError:
        raise IOError("Le fichier {} n'a pas été correctement ouvert".format(fichier_donnee_cluster_igs14))

    station_core = []
    for cluster in lines_cluster_igs14_core_data:
        # cluster est un chaine de caratère correspondant à une ligne donnée du fichier core
        # on supprime le \n (saut à la lingne) dans cluster
        cluster = cluster[:len(cluster)- 1]
        # on sépare au niveau des espaces
        cluster = cluster.split(" ")
        station_core.append(cluster)
    # création du fichier du sous réseau core
    try:
        with open(fichier_sortie, 'w') as core_file:
            for cluster in station_core:
                i = 0
                station = cluster[i]
                present = False
                nb_station_dans_cluster = len(cluster)
                while not present:
                    for igs_sation in lines_igs14_data:
                        if station in igs_sation:
                            core_file.write(igs_sation)
                            present = True
                            break
                    if present:
                        #si l'un des sations du cluster est présent parmis les sations du #jour, on sort du bouble while 
                        #et passe au cluster suivant
                        break
                    elif not present and i != nb_station_dans_cluster - 1:
                        i += 1
                        station = cluster[i]
                    elif i == nb_station_dans_cluster - 1:
                        break
                    
    except IOError:
        raise IOError("Le fichier {} n'a pas été correctement crée".format(fichier_sortie))



