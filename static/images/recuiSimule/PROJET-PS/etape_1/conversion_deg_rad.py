# -*- coding: utf-8 -*-

"""
created on:

@author: Juliette

"""

import numpy as np


def ConversionEnRad(coord_deg):
    """
    Fonction qui convertit un angle en degré en radian

    :param coord_deg: vecteur contenant des angles en degré
    :param type: np.array
    :return coord_rad: vecteur contenant ces mêmes angles convertis en radian
    :rtype: np.array

    """
    coord_rad = np.pi*coord_deg/180
    return coord_rad
