# -*- coding:utf-8 -*-

"""
created on wednesday, 13th january 2021

@author: GT

"""
import numpy as np

def BilanPresence(jours_de_presence):
    """
    calcul les présences (en jours) minimale, moyenne et maximale

    :param jours_de_presence: nombre de jours de présence de chaque station dans les solutions de l’IGS
    :type jours_de_presence: 1d-array

    :return: la valeur minimale,maximale et moyenne des présences
    :rtype: float

    """
    return(np.min(jours_de_presence), round(np.mean(jours_de_presence),0), np.max(jours_de_presence))