# -*- coding: utf-8 -*-

"""
ceated on umuary 2021

@author: GT & Juliette

"""

from time import time
from etape_1.conversion_deg_rad import ConversionEnRad
import math
import scipy.stats as sc


def Valeur_emp_DBKS(list_lat, list_long):
  """
  Fonction qui calcule les valeurs empiriques de DBKS

  :param list_lat: liste contenant les latitudes de chaque station d'un sous-reseau considere
  :param type: list
  :param list_long: liste contenant les longitudes de chaque station d'un sous-reseau considere
  :param type: list
  :return listeDBKS: liste des valeurs empirique de DBKS pour les quatres cas considérés (inf , suplong, suplat, sup)
  :rtype: list
  """
  
  L = len(list_lat)
  listeDBS = []

  list_long_norm = [ long/180 for long in list_long]
  list_lat_norm = [ math.sin(lat) for lat in list_lat]
  

  for i in range(L):
    
    inf = 0 #compteur du nb de stations dont les coordonnees sont inferieures aux deux coordonnées de la station consideree
    sup = 0 #compteur du nb de stations dont les coordonnees sont superieures aux coordonnees de la station consideree
    suplat = 0 #compteur du nb de stations dont la latitude/longitude est superieure/inferieure a celle de la station consideree
    suplong = 0 #compteur du nb de stations dont la latitude/longitude est inferieure/superieure a celle de la station considere

    X = list_long_norm[i]
    Y = list_lat_norm[i]

    for j in range(L):      

      if i == j:
        continue
      elif list_lat_norm[j] <= Y and list_long_norm[j] <= X:
        inf += 1.
      elif list_lat_norm[j]>Y and list_long_norm[j] > X:
        sup += 1.
      elif list_lat_norm[j] > Y and list_long_norm[j] <= X:
        suplat +=1.
      elif list_lat_norm[j] <= Y and list_long_norm[j] > X:
        suplong +=1.
      
    inf = (inf+1)/L
    sup = sup/L
    suplat = suplat/L
    suplong = suplong/L
      
    listeDBS.append([inf, sup, suplat, suplong])

  return listeDBS






def DBKS_valeurs_th(list_lat, list_long):
  """
  Fonction qui calcule les valeurs théoriques de DBKS

  :param list_lat: liste contenant les latitudes de chaque station d'un sous-reseau considere
  :param type: list
  :param list_long: liste contenant les longitudes de chaque station d'un sous-reseau considere
  :param type: list
  :return listeDBKS_th: liste des valeurs théoriques de DBKS pour chacun des quatres cas considérés dans la fonction empirique (inf, suplong, suplat, sup) dans le cas où la distribution est uniforme
  :rtype: list
  """
  L = len(list_lat)
  listeDBKS_th = []
  
  for i in range(L):    

    
    Y = math.sin(list_lat[i])
    X = list_long[i]/180
    
    inf_th = sc.uniform(loc=-1, scale=2).cdf([X]) * sc.uniform(loc=-1, scale=2).cdf([Y])
    suplong_th = (1 - sc.uniform(loc=-1, scale=2).cdf([X])) * sc.uniform(loc=-1, scale=2).cdf([Y])
    suplat_th = sc.uniform(loc=-1, scale=2).cdf([X]) * (1 - sc.uniform(loc=-1, scale=2).cdf([Y]))
    sup_th = (1 - sc.uniform(loc=-1, scale=2).cdf([X]))* (1 - sc.uniform(loc=-1, scale=2).cdf([Y]))
    
    #scipy.stats.uniform(-1,2)

    listeDBKS_th.append([inf_th, sup_th, suplat_th, suplong_th])

  return listeDBKS_th
  

def DBKS(array_long, array_lat,  precision):
  """
  Fonction qui à partir des vecteurs des latitudes et des longitudes, calcule la variable de décision du test Kolmogorov-Smirgov.

  :param array_long: vecteur contenant les longitudes de chaque station du réseau considéré
  :param type: np.array
  :param array_lat: vecteur contenant les latitudes de chaque station du réseau considéré
  :param type: np.array
  :param precision: precision après la virgule
  :param type: int
  :return DBKS: variable de décision du test
  :rtype: float

  """
  #tic = time()

  array_lat_rad = ConversionEnRad(array_lat)
  

  list_lat_rad, list_long = list(array_lat_rad), list(array_long)
  liste_DBKS= Valeur_emp_DBKS(list_lat_rad, list_long)
  liste_DBKS_th = DBKS_valeurs_th(list_lat_rad, list_long)
  L = len(list_lat_rad)
  liste_maxi = []

  for i in range(L):
    for j in range(4):

      list_diff = []

      diff = abs(float(liste_DBKS[i][j]) - float(liste_DBKS_th[i][j]))
      
      list_diff.append(diff)

      maxi = max(list_diff)
      
      liste_maxi.append(maxi)
      
  
  DBKS = max(liste_maxi)

  #toc = time()
  #print(toc-tic, "sec")

  return round(DBKS, precision)