# -*- coding:utf-8 -*-

"""
created on wednesday, 13th january 2021

@author: GT

"""
import numpy as np
from etape_1.conversion_deg_rad import ConversionEnRad

def DistanceSpherique(lambda_A_deg, phi_A_deg, lambda_B_deg, phi_B_deg):
    """
    calcule la distance sphérique entre 2 points A et B dont le résultat est en mètre
    en utilisant le demi petit axe comme rayon de la terre

    :param (lambda_A_deg, phi_A_deg): coordonnées du point A en degré
    :type (lambda_A_deg, phi_A_deg): float

    :param (lambda_B_deg, phi_B_deg): coordonées du point B en degré
    :type (lambda_B_deg, phi_B_deg): float

    :return: la distance sphérique entre A et B en mètre
    :rtype: float

    """
    # Conversion en radian
    lambda_A_rad = ConversionEnRad(lambda_A_deg)
    phi_A_rad = ConversionEnRad(phi_A_deg)
    lambda_B_rad = ConversionEnRad(lambda_B_deg)
    phi_B_rad = ConversionEnRad(phi_B_deg)

    d_lambda = lambda_B_rad - lambda_A_rad
    # calcule de l'élément dans arccos
    in_arcos  = np.sin(phi_A_rad)*np.sin(phi_B_rad) + np.cos(phi_A_rad)*np.cos(phi_B_rad)*np.cos(d_lambda)
    # disjonction des cas pour éviter de sortir du domaine de definition de arccos
    if in_arcos >= 1:
        in_arcos = 1
    elif in_arcos <= -1:
        in_arcos = -1

    return (np.arccos(in_arcos) * 6378137)#[mètre]

def DistanceSpheriqueMinMax(longitudes_deg, latitudes_deg, precision):
    """
    renvoie la distance sphérique minimale et maximale parmis les stations de coordonnées longitudes_deg, latitudes_deg

    :param longitudes_deg: les coordonnées en longitude des stations en degré
    :type longitudes_deg: 1d- array

    :param latitudes_deg: les coordonnees en latitude des stations en degré
    :types latitudes_deg: 1d- array

    :param precision: le nombre de chiffre après la virgule souhaiter pour les réusltats
    :type precision: int

    :return: la distance sphérique minimal et maximal en km
    :rtype: float

    """
    (n, p) = longitudes_deg.shape

    liste_distance_spherique = []
    # calule de distance sphérique entre chaque sation
    # pour une station A donnée, on calcule les distances sphériques avec tous les autres stations
    coordonnees = np.hstack((longitudes_deg, latitudes_deg))#nous évitera d'utiliser la fonction zip
    for (lambda_A_deg, phi_A_deg) in coordonnees:
        #print(lambda_A_deg, phi_A_deg)
        #ind = 0 #indice permettant de savoir qu'on a bien parcouru tous les sations
        for i in range(n):
            lambda_B_deg, phi_B_deg = longitudes_deg[i,0], latitudes_deg[i,0]
            if (lambda_A_deg, phi_A_deg) != (lambda_B_deg, phi_B_deg):
                distance_spherique = DistanceSpherique(lambda_A_deg, phi_A_deg,
                lambda_B_deg, phi_B_deg)
                liste_distance_spherique.append(distance_spherique)
            #else:
                #ind +=1
    #print(ind)
    return(round(min(liste_distance_spherique)*0.001, precision),
    round(max(liste_distance_spherique)*0.001, precision))



if __name__ == "__main__":
    #-- exemple du pdf sur le calcule de a distance S --#
    lambda_A_deg = 0
    phi_A_deg = 45
    lambda_B_deg = (1 + (50/60) + 03.156468/3600)
    phi_B_deg = (46 + (15/60) + (28.463641/3600))
    S = DistanceSpherique(lambda_A_deg, phi_A_deg, lambda_B_deg, phi_B_deg)#[mètre]
    print(round(S,1),"m")
    #print(round(S * 0.001,1),"km")





