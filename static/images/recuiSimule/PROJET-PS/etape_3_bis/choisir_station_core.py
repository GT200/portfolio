# -*- coding: utf-8 -*-

import numpy as np


def FichierCore20(fichier_igs_14, array_fobjectif, sous_reseau, fichier_sortie):
    """
    crée un fichier de sous réseau core en prenant le sous réseau correspondant le minimun de l'array_fobjectif 
    :param fichier_igs_14: fichier contenant le reseau global
    :type fichier_igs_14: file, extension: dat
    ;param array_fobjectif: array contenant le fonction objectif issu du recuit
    :type array_fobjectif: nd-array
    ;param station: array contenant les sous réseau issu du recuit
    :type station: nd-array
    :param fichier_sortie: fichier de sortie contenant le sous réseau séléctionner
    :type fichier_sortie: file, extension: dat
    
    """

    with open(fichier_igs_14, 'r') as stations_igs:
        lines_stations = stations_igs.readlines()
    
    ligne_meilleur_f_o = list(np.where(array_fobjectif == np.min(array_fobjectif))[0])[0]

    meilleur_reseau = list(np.squeeze(sous_reseau[ligne_meilleur_f_o, :]))
    
    with open(fichier_sortie, 'w') as stations_core:
        for num_ligne in meilleur_reseau:
            stations_core.write(lines_stations[num_ligne] + "\n")
    

        
    













    


