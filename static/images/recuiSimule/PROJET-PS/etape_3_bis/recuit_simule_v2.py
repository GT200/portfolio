# -*- coding: utf-8 -*-
"""

"""
import numpy as np
from math import log, exp
import json

from etape_1.distance_spherique import DistanceSpheriqueMinMax
from etape_1.presence_en_jours import BilanPresence
from etape_1.creer_fichier_core import ExtraireCore
from etape_1.dbks2 import DBKS
from etape_2.normalisation import NormalisationDeCritere
from etape_3_bis.temperature_initiale_v2 import CreationSousReseau


def TemperatureRecuitV2(fichier_igs_14, fichier_sortie, ds_min, ds_max, pr_min, pr_max, nb_variation, taux, T, precision):
    """
    Génère et crée un nombre "nb_variation" possibilité de sous réseau core pour chaque itération du recuit simulé (dans cette version le vecteur de sous-réseau n'est plus un vecteur de 0 ou de 1 mais contient des numéros de lignes)

    :param fichier_igs_14: nom du fichier IGS14 pour le jour considérée
    :type fichier_donnee_igs14: file, extention: dat
    :param fichier_sortie: fichiers contenant la trace du réseau ayant servi à calculer la température initiale
    :type fichier_sortie: file, extention: dat
    :param (ds_min, ds_max, pr_min, pr_max): distance min/max, pésence min/max pour l'ensemble du réseau considéré
    :type (ds_min, ds_max, pr_min, pr_max): float
    :param nb_variation: nombre de variation
    :type nb_variation: int
    :param taux: taux d'acceptation 
    :type taux: float
    :param T: tempetature permattant de calculer la probabilité d'acceptation des fonction objectif dégradant
    :type T: float
    :param precision: preciser le nombre de chiffre après la virgule
    :type precision: int
    :return: Température initiale et les critères
    :rtype: tuple
    
    """
    data = np.genfromtxt(fichier_igs_14)
    (n, p) = data.shape
    liste_des_sr = [CreationSousReseau(n/2, n)]

    # liste des critères et variables utiles
    l_f_objectif = []
    l_f_objectif_degrade = []
    l_ds_morm = []
    l_presense_norm = []
    l_dbks_norm = []
    m1, m2 = 0, 0

    trace = open(fichier_sortie, 'a')

    for variation in range(nb_variation):
        sous_reseau = np.delete(data
, liste_des_sr[-1], 0)
        longitudes_deg_sous_reseau = sous_reseau [:, 1:2]
        latitudes_deg_sous_reseau  = sous_reseau [:, 2:3]
        jours_de_presence_sous_reseau  = sous_reseau [:, 3:4]
        # calule des critères
        (ds_min_core, ds_max_core) = DistanceSpheriqueMinMax(longitudes_deg_sous_reseau, latitudes_deg_sous_reseau, precision)
        presence_min_core, presence_moyenne_core, presence_max_core = BilanPresence(jours_de_presence_sous_reseau)
        dbks_core = DBKS(longitudes_deg_sous_reseau, latitudes_deg_sous_reseau, precision)

         # normalisation 
        ds_min_norm = NormalisationDeCritere(ds_max, ds_min, ds_min_core, precision)
        presence_moyenne_norm = NormalisationDeCritere(pr_max, pr_min, presence_moyenne_core, precision)
        dbks_norm = NormalisationDeCritere(0, 0.5, dbks_core, precision)

        l_ds_morm.append(ds_min_norm)
        l_presense_norm.append(presence_moyenne_norm)
        l_dbks_norm.append(dbks_norm)
        
    
        # mise à jour des variables de calcul de la temperature initiale
        f_objectif = (1/3) * (ds_min_norm + presence_moyenne_norm + dbks_norm)
        # plus de penalite ici
        if len(l_f_objectif) != 0:
            if f_objectif - l_f_objectif[-1] < 0:
                m1 += 1
                l_f_objectif.append(f_objectif)
            else:
                proba = exp((l_f_objectif[-1] - f_objectif)/T)
                r = np.random.random()
                if r < proba:
                    m2 += 1
                    l_f_objectif.append(f_objectif)
                    l_f_objectif_degrade.append(f_objectif)
                
        # garder les traces dans un fichier
        list_trace = [ds_min_norm, presence_moyenne_norm, dbks_norm,round(f_objectif, precision), len(liste_des_sr[-1])]
        
        trace.write(str(list_trace) + "\n")
        trace.write(str(liste_des_sr[-1]) + "\n")

        # mis à jour de "data" en passant à une autre variation possible de sous réseau core
        lignes_sous_reseau = CreationSousReseau(n/2, n)
        while lignes_sous_reseau in liste_des_sr:
            lignes_sous_reseau = CreationSousReseau(n/2, n)
        else:
            liste_des_sr.append(lignes_sous_reseau)

        #print("*"* 20)
        print("variation numéro:",variation)

    #trace.write(str(list(data[:, 0])) + "\n")
    trace.close()

def RecuitSimuleV2(fichier_igs_14, fichier_sortie, ds_min, ds_max, pr_min, pr_max, nb_variation, taux, T, alpha, precision):
    """
    Algorithme permettant de trouver un sous-réseau optimal en refroidissant petit à petit la température initiale (suivant le même processus que le refroidissement d'un métal).

    :param fichier_igs_14: nom du fichier IGS14 pour le jour considérée
    :type fichier_donnee_igs14: file, extention: dat
    :param fichier_sortie: fichiers contenant la trace du réseau ayant servi à calculer la température initiale
    :type fichier_sortie: file, extention: dat
    :param (ds_min, ds_max, pr_min, pr_max): distance min/max, pésence min/max pour l'ensemble du réseau considéré
    :type (ds_min, ds_max, pr_min, pr_max): float
    :param nb_variation: nombre de variation
    :type nb_variation: int
    :param taux: taux d'acceptation 
    :type taux: float
    :param T: tempetature permettant de calculer la probabilité d'acceptation des fonction objectif dégradant
    :type T: float
    :param precision: preciser le nombre de chiffre après la virgule
    :type precision: int
    :param alpha: la raison de la suite géométrique permettant de calculer la température à l'instant t + 1
    :type alpha: float 
    :return: Température initiale et les critères
    :rtype: tuple
    """
    iteration = 0
    T0 = T
    eps = T0/100
    while T > eps:
        TemperatureRecuitV2(fichier_igs_14, fichier_sortie, ds_min, ds_max, pr_min, pr_max, nb_variation, taux, T, precision)

        T = alpha * T
      
        iteration += 1
        print(T, eps, iteration)




def ExtractTrace(fichier_trace):
    """
    Fonction qui récupère les traces des calculs sous forme de matrices colonnes
    
    :param fichier_trace: nom du fichier contenant les traces du recuit simul"
    :type fichier_trace:file, extension; dat
    
    :return ds, pr, dbks, f_o, table_sous_reseau: matrice colonne de chaque critère/ les sous réseaux correspondant que l'on veut récupérer
    :rtype: tuple
    
    """

    with open(fichier_trace, 'r') as data_trace:
        lines_data_trace = data_trace.readlines()
    liste_critere = []
    liste_sous_reseau = []
    i = 1
    for trace in lines_data_trace:
        if i % 2 != 0:
            liste_critere.append(json.loads(trace.replace("\n", "")))
        else:
            liste_sous_reseau.append(json.loads(trace.replace("\n", "")))
        i += 1
  
    table_critere = np.array(liste_critere, dtype = float).reshape(len(liste_critere), len(liste_critere[0]))
    table_sous_reseau = np.array(liste_sous_reseau, dtype = int).reshape(len(liste_sous_reseau), len(liste_sous_reseau[0]))
    print(len(liste_critere), len(liste_sous_reseau), len(lines_data_trace))

    ds = table_critere[ : , 0:1]
    pr = table_critere[ : , 1:2]
    dbks = table_critere[ : , 2:3]
    f_o = table_critere[ : , 3:4]

    return (ds, pr, dbks, f_o, table_sous_reseau)




