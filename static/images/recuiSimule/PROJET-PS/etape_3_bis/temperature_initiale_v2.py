import numpy as np
from math import log, exp

from etape_1.distance_spherique import DistanceSpheriqueMinMax
from etape_1.presence_en_jours import BilanPresence
from etape_1.creer_fichier_core import ExtraireCore
from etape_1.dbks2 import DBKS
from etape_2.normalisation import NormalisationDeCritere


def CreationSousReseau(nb_sous_reseau, nb_station):
    """
    crée un un vecteur de 10 entiers distincts, chaque entier correspondant à l’indice d’une station dans la liste de 20 stations possibles

    :param nb_sous_reseau: ombre de sous réseau rechercher (ici 10)
    :type nb_sous_reseau: int
    :param nb_station: nombre de station au total (ici 20)
    :type nb_station: int
    :return: liste de sous réseau sous forme de numéro de ligne par rapport au réseau global (de 20 station ici)
    :rtype: list

    """
    liste_sr = []
    i = 0
    while i !=nb_sous_reseau:
        ligne = np.random.randint(nb_station)
        if ligne not in liste_sr:
            liste_sr.append(ligne)
            i += 1
    return(liste_sr) 



def TemperatureInitialeV2(fichier_igs_14, fichier_sortie, ds_min, ds_max, pr_min, pr_max, nb_variation, taux, precision):
    """
    crée un nombre "nb_variation" possibilité de sous réseau core pour détermier la température initiale du recuit simulé et garde les traces dans fichier_sortie (dans cette version le vecteur de sous-réseau n'est plus un vecteur de 0 ou de 1 mais contient des numéros de lignes)

    :param fichier_igs_14: nom du fichier IGS14 pour le jour considérée
    :type fichier_donnee_igs14: file, extention: dat
    :param fichier_sortie: fichiers contenant la trace du réseau ayant servi à calculer la température initiale
    :type fichier_sortie: file, extention: dat
    :param (ds_min, ds_max, pr_min, pr_max): distance min/max, pésence min/max pour l'ensemble du réseau considéré
    :type (ds_min, ds_max, pr_min, pr_max): float
    :param nb_variation: nombre de variation
    :type nb_variation: int
    :param taux: taux d'acceptation 
    :type taux: float
    :param precision: preciser le nombre de chiffre après la virgule
    :type precision: int
    :return: Température initiale et les critères
    :rtype: tuple
    
    """
    data = np.genfromtxt(fichier_igs_14)
    (n, p) = data.shape
    liste_des_sr = [CreationSousReseau(n/2, n)]

    # liste des critères et variables utiles 
    l_f_objectif = []
    l_f_objectif_degrade = []
    l_ds_morm = []
    l_presense_norm = []
    l_dbks_norm = []
    m1, m2 = 0, 0

    trace = open(fichier_sortie, 'a')

    for variation in range(nb_variation):
        sous_reseau = np.delete(data, liste_des_sr[-1], 0)
        longitudes_deg_sous_reseau = sous_reseau [:, 1:2]
        latitudes_deg_sous_reseau  = sous_reseau [:, 2:3]
        jours_de_presence_sous_reseau  = sous_reseau [:, 3:4]
        # calule des critères
        (ds_min_core, ds_max_core) = DistanceSpheriqueMinMax(longitudes_deg_sous_reseau, latitudes_deg_sous_reseau, precision)
        presence_min_core, presence_moyenne_core, presence_max_core = BilanPresence(jours_de_presence_sous_reseau)
        dbks_core = DBKS(longitudes_deg_sous_reseau, latitudes_deg_sous_reseau, precision)

         # normalisation 
        ds_min_norm = NormalisationDeCritere(ds_max, ds_min, ds_min_core, precision)
        presence_moyenne_norm = NormalisationDeCritere(pr_max, pr_min, presence_moyenne_core, precision)
        dbks_norm = NormalisationDeCritere(0, 0.5, dbks_core, precision)

        l_ds_morm.append(ds_min_norm)
        l_presense_norm.append(presence_moyenne_norm)
        l_dbks_norm.append(dbks_norm)
        
    
        # mise à jour des variables de calcul de la temperature initiale
        f_objectif = (1/3) * (ds_min_norm + presence_moyenne_norm + dbks_norm)
        # plus de penalite ici
        if len(l_f_objectif) != 0:
            if f_objectif - l_f_objectif[-1] < 0:
                m1 += 1
            else:
                m2 += 1
                l_f_objectif_degrade.append(f_objectif)
        l_f_objectif.append(f_objectif)
                
        # garder les traces dans un fichier
        list_trace = [ds_min_norm, presence_moyenne_norm, dbks_norm, round(f_objectif, precision), len(liste_des_sr[-1])]
        
        trace.write(str(list_trace) + "\n")
        trace.write(str(liste_des_sr[-1]) + "\n")

        # mis à jour de "data" en passant à une autre variation possible de sous réseau core

        lignes_sous_reseau = CreationSousReseau(n/2, n)
        while lignes_sous_reseau in liste_des_sr:
            lignes_sous_reseau = CreationSousReseau(n/2, n)
        else:
            liste_des_sr.append(lignes_sous_reseau)

        print("variation numéro:",variation)

    n = len(l_f_objectif_degrade)
    m = sum(l_f_objectif_degrade)/n
    delta_f = sum([ abs(f - m) for f in l_f_objectif_degrade])/n

    # calule de T0 
    
    T0 = delta_f/log(m2/(m2 * taux - m1 * (1 - taux)))

    trace.close()

    return(T0, l_ds_morm, l_presense_norm, l_dbks_norm, l_f_objectif)
