# -*- coding: utf-8 -*-

"""
created on sunday, december 13th 2020

@author: GT

"""
import numpy as np

from mpl_toolkits.mplot3d import axes3d

import matplotlib.pyplot as plt

from matplotlib import cm

def Affichage3D(mnt_data):
    """
    affiche un modèle 3D du mnt avec un pas de 1 en x et y

    :param mnt_data: les données terrain en altitude
    :type mnt_data: 2D-array

    """

    (n , p) = mnt_data.shape

    x_coords = np.linspace(tuple(np.ones((p,))), tuple(np.ones((p,))*n), n)
    y_coords = np.linspace(tuple(np.arange(1, p+ 1)),tuple(np.arange(1, p+ 1)), n)
    z_coords = mnt_data

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('Altitude')

    surf = ax.plot_surface(x_coords, y_coords, z_coords, cmap=cm.jet,  alpha = 0.9)

    cbar = fig.colorbar(surf, shrink=0.3, aspect=5)
    cbar.ax.set_title("Altitude")

    plt.show()


def Affichage2D(mnt_data):
    """
    affiche une image 2D des altitudes

    :param mnt_data: les données terrain en altutude
    :type mnt_data: array


    """

    im = plt.matshow(mnt_data, cmap = cm.jet)
    cbar = plt.colorbar(im, shrink=0.5, aspect=10)
    cbar.ax.set_title('Altidude')

    plt.show()



