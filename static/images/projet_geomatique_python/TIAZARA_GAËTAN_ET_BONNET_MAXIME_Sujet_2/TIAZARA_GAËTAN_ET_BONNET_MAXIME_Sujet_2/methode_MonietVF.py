# -*- coding: utf-8 -*-

"""
created on thursday, december 17th 2020

@author:GT

"""

import numpy as np

import matplotlib.pyplot as plt

from matplotlib import cm

from termcolor import cprint

def ExtractMNTPart(mnt_data, ligne_sup, ligne_inf, colonne_gauche, colonne_droit):
    """
    extrait une portion rectangulaire du mnt initial

    :param mnt_data: la matrice du mnt d'où on extrait une portion
    :type mnt_data: array
    :param ligne_sup: début de ligne à extraire
    :type ligne_sup: int
    :param ligne_inf: fin de ligne à extraire
    :type ligne_inf: int
    :param colonne_gauche: début de colonne à extraire
    :type colonne_gauche: int
    :param colonne_droite: fin de colonne à extraire
    :type colonne_droite: int
    :return: un portion du mnt
    :rtype: array

    """

    extrait = mnt_data[ligne_sup:ligne_inf + 1, colonne_gauche:colonne_droit + 1]

    return(extrait)


def VoisinageMinMax(mnt_data, i, j):
    """
    cherche parmis les 8 voisins( au plus ) de la case i, j, le minimun et le maximun des altidudes. Paraît long mais le but était de ne pas perdre l'indice du min et du max par rapport à mnt_data( la grande matrice ) permettant de les localiser pécisément dans la suite.

    :param mnt_data: la matrice du mnt d'où on cherchera les voisins d'une case
    :type mnt_data: array
    :param i, j: la ligne, colonne
    :type i, j: 2 int
    :return: les indices et les valeurs des voisin minimun et maximun
    :rtype: list

    """
    (n, p) =  mnt_data.shape
    if i == 0:
        if j == 0:
            #matrice_de_voisininage = mnt_data[i: i + 2, j: j + 2]
            imin, jmin, imax, jmax = i, j + 1, i, j + 1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i, i + 2):
                for j_voisinage in range(j, j + 2):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

        elif j == p - 1:
            #matrice_de_voisininage = mnt_data[i: i + 2, j - 1: j + 1]
            imin, jmin, imax, jmax = i, j - 1, i, j - 1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i, i + 2):
                for j_voisinage in range(j - 1, j + 1):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

        else:
            #matrice_de_voisininage = mnt_data[i: i + 2, j - 1: j + 2]
            imin, jmin, imax, jmax = i, j - 1, i, j - 1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i, i + 2):
                for j_voisinage in range(j - 1, j + 2):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

    elif i == n - 1:
        if j == 0:
            #matrice_de_voisininage = mnt_data[i  -1: i + 1, j: j + 2]
            imin, jmin, imax, jmax = i - 1, j, i- 1 , j
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i - 1, i + 1):
                for j_voisinage in range(j, j + 2):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

        elif j == p - 1:
            #matrice_de_voisininage = mnt_data[i - 1: i + 1, j - 1: j + 1]
            imin, jmin, imax, jmax = i - 1, j - 1, i - 1, j - 1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i - 1, i + 1):
                for j_voisinage in range(j - 1, j + 1):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

        else:
            #matrice_de_voisininage = mnt_data[i - 1: i + 1, j - 1: j + 2]
            imin, jmin, imax, jmax = i - 1, j - 1, i - 1, j - 1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i - 1, i + 1):
                for j_voisinage in range(j - 1, j + 2):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

    elif j == 0:
        #matrice_de_voisininage = mnt_data[i - 1: i + 2, j: j + 2]
            imin, jmin, imax, jmax = i - 1, j, i - 1, j
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i - 1, i + 2):
                for j_voisinage in range(j, j + 2):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

    elif j == p-1:
        #matrice_de_voisininage = mnt_data[i - 1: i + 2, j - 1: j + 1]
            imin, jmin, imax, jmax = i - 1, j - 1, i - 1, j -1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i - 1, i + 2):
                for j_voisinage in range(j - 1, j + 1):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

    else:
        matrice_de_voisininage = mnt_data[i - 1: i + 2, j - 1: j + 2]
        matrice_de_voisininage = np.delete(matrice_de_voisininage, 4)
        minimun, maximum = np.min(matrice_de_voisininage), np.max(matrice_de_voisininage)
        lmin, lmax = [], []
        imin, jmin, imax, jmax = i - 1, j - 1, i - 1, j - 1
        for i_voisinage in range(i - 1, i + 2):
            for j_voisinage in range(j - 1, j + 2):
                if (i_voisinage, j_voisinage) != (i, j):
                    if mnt_data[i_voisinage, j_voisinage] == minimun:
                        lmin.append((i_voisinage, j_voisinage))

                    if mnt_data[i_voisinage, j_voisinage] == maximum:
                        lmax.append((i_voisinage, j_voisinage))

        if len(lmin) >= 2:
            (imin, jmin) = lmin[1]
        else:
            (imin, jmin) = lmin[0]
        if len(lmax) >= 2:
            (imax, jmax) = lmax[1]
        else:
            (imax, jmax) = lmax[0]
        voisin_min = minimun
        voisin_max = maximum

    #print(matrice_de_voisininage)

    return([[(imin, jmin), (imax, jmax)], (voisin_min, voisin_max)])



def DicoVoisinageMinMax(mnt_data):
    """
    associe à chaque case de la donnée mnt le voisin à l'atitude max et min

    :param mnt_data: la matrice du mnt où on associera les voisins min et max de chaque case
    :type mnt_data: array
    :return:

    """
    #print("dico lancé")
    (n, p) = mnt_data.shape
    dico_voisin_min_max = {}
    for i in range(n):
        for j in range(p):
            dico_voisin_min_max[(i,j)] = VoisinageMinMax(mnt_data, i, j)[0]

    return(dico_voisin_min_max)

def SimulationEcoulement(mnt_data, mode = "crete"):
    """
    simule l'écoulement d'une goutte d'eau pour chaque case

    :param mnt_data: les donnée altitudes où on simulera l'éoulement d'une goutte d'eau
    :type mnt_data: array
    :mode: choisi le calclul de la matrice d'écoulement en mode talweg ou crête
    :type: str

    """
    (n, p) = mnt_data.shape
    matrice_d_ecoulement = np.zeros((n,p))
    dico_min_max = DicoVoisinageMinMax(mnt_data)
    if mode == "talweg":
        for i in range(n):
            print("NUMERO DE LA LIGNE PARCOURUE ACTUELLEMNT: {}".format(i))
            #cprint("NUMERO DE LA LIGNE PARCOURUE ACTUELLEMNT: {}".format(i), "green")
            for j in range(p):
                #cprint("NUMERO DE LA COLONNE PARCOURUE ACTUELLEMNT: {}".format(j), "green")
                i_actuelle, j_actuelle = i, j
                (i_voisin_min, j_voisin_min) = dico_min_max[(i_actuelle, j_actuelle)][0]
                while mnt_data[i_actuelle, j_actuelle] > mnt_data[i_voisin_min, j_voisin_min]:
                    #print( mnt_data[i_actuelle, j_actuelle], mnt_data[i_voisin_min, j_voisin_min])
                    matrice_d_ecoulement[i_voisin_min, j_voisin_min] += 1
                    i_actuelle, j_actuelle = i_voisin_min, j_voisin_min
                    i_voisin_min, j_voisin_min = dico_min_max[(i_actuelle, j_actuelle)][0]
    else:
        for i in range(n):
            print("NUMERO DE LA LIGNE PARCOURUE ACTUELLEMNT: {}".format(i))
            #cprint("NUMERO DE LA LIGNE PARCOURUE ACTUELLEMNT: {}".format(i), "green")#avoir une idée de ou en ai l'algorithme, y a t-il une boucle infinie?
            for j in range(p):
                #cprint("NUMERO DE LA COLONNE PARCOURUE ACTUELLEMNT: {}".format(j), "green")#avoir une idée de ou en ai l'algorithme, y a t-il une boucle infinie?
                i_actuelle, j_actuelle = i, j
                (i_voisin_max, j_voisin_max) = dico_min_max[(i_actuelle, j_actuelle)][1]
                while mnt_data[i_actuelle, j_actuelle] < mnt_data[i_voisin_max, j_voisin_max]:
                    #print( mnt_data[i_actuelle, j_actuelle], mnt_data[i_voisin_max, j_voisin_max])
                    matrice_d_ecoulement[i_voisin_max, j_voisin_max] += 1
                    i_actuelle, j_actuelle = i_voisin_max, j_voisin_max
                    i_voisin_max, j_voisin_max = dico_min_max[(i_actuelle, j_actuelle)][1]

    #print(matrice_d_ecoulement)
    im = plt.matshow(matrice_d_ecoulement, cmap = cm.jet)
    plt.title("MATRICE D'ECOULEMENT")
    cbar = plt.colorbar(im, shrink=0.5, aspect=10, ticks = [0, np.max(matrice_d_ecoulement)])
    cbar.ax.set_yticklabels(['écoulement', 'équilbre\n' + str(np.max(matrice_d_ecoulement))])
    #plt.savefig("fig_matrice_d_ecoulement.png")
    #plt.imsave("image_matrice_d_ecoulement.png", matrice_d_ecoulement, cmap = cm.jet)
    #np.save("matrice_d_ecoulement.npy", matrice_d_ecoulement)
    plt.show()
    return(matrice_d_ecoulement)

def ZoneInteressante(matrice_d_ecoulement):
    """
    cherche et représente au mieux les ligne de crête et les talweg

    :param matrice_d_ecoulement: matrice_d_ecoulement obtenue avec la fonction Simulation écoulement
    :type matrice_d_ecoulement: 2D array
    """
    #les partie mis en commentaire ont permis d'avoir les images discontinue de ligne de crête et talweg (les images V1)
    m = matrice_d_ecoulement
    #smin = 0.15
    #seuil_min = np.max(m) * smin #on ne gardera que les valeurs supérieur à x% (ici 2 %) de la valeur maximal dans la matrice d'écoulement
    #zone_speciciale = np.where( m <= seuil_min, 1, 0)#on réduit à 0 les valures inférieur smin*100 % dans la

    zone_speciciale = np.where( m == 0, m, 1)#on réduit à 0 les valures inférieur smin*100 % dans la matrice d'écoulement.

    im = plt.matshow(zone_speciciale, cmap = cm.jet)
    plt.colorbar(im, shrink=0.5, aspect=10)
    plt.title("MATRICE D'ECOULEMENT TRIEE")
    #plt.savefig("fig_zone_interessant.png")
    #plt.imsave("image_zone_interessant.png", zone_speciciale, cmap = cm.jet)

    plt.show()

