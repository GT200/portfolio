import numpy as np
import matplotlib.pyplot as plt
from file_reader import openAsc
from affiche_2D_3D import Affichage2D, Affichage3D
from methode_MonietVF import ExtractMNTPart, VoisinageMinMax, DicoVoisinageMinMax, SimulationEcoulement, ZoneInteressante


from termcolor import cprint

if __name__ == "__main__":

    #-- extracting data from asc file--#
    data_file = "mnt_5m.asc"

    all_data = openAsc(data_file)

    alt_data = all_data["data"]

    (n, p) = alt_data.shape

    #-- showing data --#

    choice = int(input("Afficher le Modèle 2D?(1 oui, 0 non): "))

    if choice == 1:
        cprint("AFFICHAGE 2D", "green")
        Affichage2D(alt_data)

    choice = int(input("Afficher le Modèle 3D?(1 oui, 0 non): "))
    if choice == 1:
        cprint("AFFICHAGE 3D", "green")
        Affichage3D(alt_data)

    #-- treating data --#

    # 1) test des fonctions #
    #test de la fonction VoisinageMinMax
    choice = int(input("Tester la fonction VoisinageMinMax?(1 oui, 0 non): "))
    if choice == 1:
        cprint("\nmatrice avec des valeur aléatoire de randint".upper(), "green")
        aleatoire = np.random.randint(1, 100, (5, 5))
        print(aleatoire)
        cprint("voisin min max des positions particulières à partir de la matrice à valeures aléatoires".upper(), "green")
        cprint("VoisinageMinMax V1".upper(), "green")
        print(VoisinageMinMax(aleatoire, 0, 0))
        print(VoisinageMinMax(aleatoire, 0, 4))
        print(VoisinageMinMax(aleatoire, 4, 0))
        print(VoisinageMinMax(aleatoire, 4, 4))
        print(VoisinageMinMax(aleatoire, 2, 2))
        #test de la fonction DicoVoinageMinMax
        choice = int(input("Tester la fonction DicoVoisinageMinMax?(1 oui, 0 non): "))
        if choice == 1:
            cprint("\nDictionaire des min et des max".upper(), "green")
            dico_min_max = DicoVoisinageMinMax(aleatoire)
            print(dico_min_max)

    #test extraction

    choice = int(input("Tester la fonction ExtractMNTPart?(1 oui, 0 non): "))
    if choice == 1:
        print("Les zones extraites sont arbitraire, changez le main pour d'autres zones")
        cprint("Extrait talweg, affichage 2D/3D", "green")
        extrait_talweg = ExtractMNTPart(alt_data, 200, 300, 100, 300 )
        Affichage2D(extrait_talweg)
        Affichage3D(extrait_talweg)
        cprint("Extrait crete, affichage 2D/3D", "green")
        extrait_crete = ExtractMNTPart(alt_data, 0, 600, 750, 1500 )
        Affichage2D(extrait_crete)
        Affichage3D(extrait_crete)

    #test SimulationEcoulement
    extrait_talweg = ExtractMNTPart(alt_data, 200, 300, 100, 300 )
    extrait_crete = ExtractMNTPart(alt_data, 0, 600, 750, 1500 )
    choice = int(input("Tester la fonction SimulationEcoulement?(1 oui, 0 non): "))
    if choice == 1:
        choice_t = int(input("Tester la fonction SimulationEcoulement sur talweg?(1 oui, 0 non):"
        ))
        if choice_t == 1:
            cprint("Extrait talweg, affichage 3D", "green")
            Affichage3D(extrait_talweg)
            cprint("Matrice d'écoulement en affichage 2D".upper(), "green")
            SimulationEcoulement(extrait_talweg, mode = "talweg")
            choice_t = int(input("Tester la fonction ZoneInteressante pour les talwegs?(1 oui, 0 non):"))
            if choice_t == 1:
                mde = SimulationEcoulement(extrait_talweg, mode = "crete")#mode crète permet de mieux voir les talwegs
                cprint("Affichage Zone ineressante")
                ZoneInteressante(mde)

        choice_c = int(input("Tester la fonction SimulationEcoulement pour les crètes ?(1 oui, 0 non): "))
        if choice_c == 1:
            cprint("Extrait crete, affichage 3D", "green")
            Affichage3D(extrait_crete)
            cprint("Matrice d'écoulement en affichage 2D".upper(), "green")
            SimulationEcoulement(extrait_crete, mode = "crete")
            choice_t = int(input("Tester la fonction ZoneInteressante sur talweg?(1 oui, 0 non):"))
            if choice_t == 1:
                mde = SimulationEcoulement(extrait_crete, mode = "talweg")#mode talweg permet de mieux voir les crètes
                cprint("Affichage Zone ineressante")
                ZoneInteressante(mde)



    # 2) application des fonctions au donnée mnt #
    #mde pour patrice d'ecoulement

    choice = int(input("APPLICATION AU DONNEE ENTIERE?(1 oui, 0 non): "))
    if choice == 1:
        #mde pour matrice d'écoulement
        print("La fonction SimulationEcoulement sera appellée 2 fois (mode crête et talweg), cela prendra environ 10min evec la version de la fonction VoisnageMinMax utiliser (5 min avec l'ancienne version mais dont la qualité de la matrice d'ecoulemnt est moindre. La version initiale de la fonction VoisnageMinMax est dans le dossier 'image et pdf' ")
        mde1 = SimulationEcoulement(alt_data, mode = "talweg")
        mde2 = SimulationEcoulement(alt_data, mode = "crete")
        ZoneInteressante(mde1)
        ZoneInteressante(mde2)

