#-- version initiale de la fonction VoisinageMimax , plus simple mais moins réaliste--# voir photo pour un exemple de son utilisation

def VoisinageMinMaxInital(mnt_data, i, j):
    """
    cherche parmis les 8 voisins( au plus ) de la case i, j, le minimun et le maximun des altidudes. Paraît long mais le but était de ne pas perdre l'indice du min et du max par rapport à mnt_data( la grande matrice ) permettant de les localiser pécisément sans la suite. à améliorer?

    :param mnt_data: la matrice du mnt où on cherchera les voisins d'une case
    :type mnt_data: array
    :param i, j: la ligne, colonne
    :type i, j: 2 int
    :return: les indices et les valeurs des voisin minimun et maximun
    :rtype: list

    """
    (n, p) =  mnt_data.shape
    if i == 0:
        if j == 0:
            #matrice_de_voisininage = mnt_data[i: i + 2, j: j + 2]
            imin, jmin, imax, jmax = i, j + 1, i, j + 1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i, i + 2):
                for j_voisinage in range(j, j + 2):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

        elif j == p - 1:
            #matrice_de_voisininage = mnt_data[i: i + 2, j - 1: j + 1]
            imin, jmin, imax, jmax = i, j - 1, i, j - 1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i, i + 2):
                for j_voisinage in range(j - 1, j + 1):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] <= voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] >= voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

        else:
            #matrice_de_voisininage = mnt_data[i: i + 2, j - 1: j + 2]
            minim = np.min(mnt_data[i: i + 2, j - 1: j + 2])
            potentiel_min = []
            potentiel_max = []
            imin, jmin, imax, jmax = i, j - 1, i, j - 1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i, i + 2):
                for j_voisinage in range(j - 1, j + 2):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

    elif i == n - 1:
        if j == 0:
            #matrice_de_voisininage = mnt_data[i  -1: i + 1, j: j + 2]
            imin, jmin, imax, jmax = i - 1, j, i- 1 , j
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i - 1, i + 1):
                for j_voisinage in range(j, j + 2):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

        elif j == p - 1:
            #matrice_de_voisininage = mnt_data[i - 1: i + 1, j - 1: j + 1]
            imin, jmin, imax, jmax = i - 1, j - 1, i - 1, j - 1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i - 1, i + 1):
                for j_voisinage in range(j - 1, j + 1):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] <= voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] >= voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

        else:
            #matrice_de_voisininage = mnt_data[i - 1: i + 1, j - 1: j + 2]
            imin, jmin, imax, jmax = i - 1, j - 1, i - 1, j - 1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i - 1, i + 1):
                for j_voisinage in range(j - 1, j + 2):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

    elif j == 0:
        #matrice_de_voisininage = mnt_data[i - 1: i + 2, j: j + 2]
            imin, jmin, imax, jmax = i - 1, j, i - 1, j
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i - 1, i + 2):
                for j_voisinage in range(j, j + 2):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

    elif j == p-1:
        #matrice_de_voisininage = mnt_data[i - 1: i + 2, j - 1: j + 1]
            imin, jmin, imax, jmax = i - 1, j - 1, i - 1, j -1
            voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
            for i_voisinage in range(i - 1, i + 2):
                for j_voisinage in range(j - 1, j + 1):
                    if (i_voisinage, j_voisinage) != (i, j):
                        if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                            voisin_min = mnt_data[i_voisinage, j_voisinage]
                            imin, jmin = i_voisinage, j_voisinage
                        if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                            voisin_max = mnt_data[i_voisinage, j_voisinage]
                            imax, jmax = i_voisinage, j_voisinage

    else:
        #matrice_de_voisininage = mnt_data[i - 1: i + 2, j - 1: j + 2]
        imin, jmin, imax, jmax = i - 1, j - 1, i - 1, j - 1
        voisin_min, voisin_max = mnt_data[imin, jmin],  mnt_data[imax, jmax]
        for i_voisinage in range(i - 1, i + 2):
            for j_voisinage in range(j - 1, j + 2):
                if (i_voisinage, j_voisinage) != (i, j):
                    if mnt_data[i_voisinage, j_voisinage] < voisin_min:
                        voisin_min = mnt_data[i_voisinage, j_voisinage]
                        imin, jmin = i_voisinage, j_voisinage
                    if mnt_data[i_voisinage, j_voisinage] > voisin_max:
                        voisin_max = mnt_data[i_voisinage, j_voisinage]
                        imax, jmax = i_voisinage, j_voisinage

    #print(matrice_de_voisininage)

    return([[(imin, jmin), (imax, jmax)], (voisin_min, voisin_max)])

