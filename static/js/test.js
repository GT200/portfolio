// Three.js ray.intersects with offset canvas
import { Ellipsoide } from "./ellipsoide.js"
import * as THREE from './lib/three.module.js';

let count = 0;

// taille de notre canva 
const CANVAS_WIDTH = 500;
const CANVAS_HEIGHT = 500;

// récupérer le conteneur div du visuel webgl  
const container = document.getElementById('ellipsoide');

// initialaisation du visuel webgl 
const renderer = new THREE.WebGLRenderer();
// paramétrage du visuel pour éviter les déformation 
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(CANVAS_WIDTH, CANVAS_HEIGHT);
// ajouter cette visuel du div 
container.appendChild(renderer.domElement);

// initilaisatind e la scene
const scene = new THREE.Scene();
// inilisation de la caméra en perspective camera 
const camera = new THREE.PerspectiveCamera(75, CANVAS_WIDTH / CANVAS_HEIGHT, 0.1, 1000);

// positionement de la caméra
// camera.position.y = 150;
// camera.position.z = 500;
// camera.lookAt(scene.position);
scene.add(camera);

// ajout de lumières 
scene.add(new THREE.AmbientLight(0x222222));
const light = new THREE.PointLight(0xffffff, 1);
camera.add(light);

//initialisation de l'éllipsoide
const ellipsoide = new Ellipsoide(THREE);
ellipsoide.draw(scene)

// Positionnement de la caméra
camera.position.z = ellipsoide.b_demi_petite_axe * 0.00021;

// Fonction d'animation
function animate() {
  requestAnimationFrame(animate);
  // Rotation de la sphère
  // sphereMesh.rotation.x += 0.01;
  ellipsoide.autoRotateY();

  renderer.render(scene, camera);
}

// Appel de la fonction d'animation
animate();