export class Ellipsoide {
  a_demi_grand_axe = 6378137.0000;
  b_demi_petite_axe = 6356752.3141;

  constructor(THREE) {
    this.e_elongation = Math.sqrt((this.a_demi_grand_axe ** 2 - this.b_demi_petite_axe ** 2) / this.a_demi_grand_axe ** 2)
    this.initDrawVariables(64, 64)
    this.computeGeometry(THREE)

  }

  computeGeometry(THREE) {
    let facteur_exageration = 0.00002
    let geometry = new THREE.SphereGeometry(this.b_demi_petite_axe, this.widthSegments, this.heightSegments);
    geometry.scale(0.0001 + (1 - (this.a_demi_grand_axe / this.b_demi_petite_axe)) * 0.0001 + facteur_exageration, 0.0001, 0.0001 + (1 - (this.a_demi_grand_axe / this.b_demi_petite_axe)) * 0.0001 + facteur_exageration)
    // Création du matériau
    var wireframeMaterial = new THREE.MeshBasicMaterial({
      color: 0x00ff00,
      wireframe: true
    });
    // Création du maillage de la sphère avec la géométrie et le matériau de fil de fer
    this.ellipsoideMesh = new THREE.Mesh(geometry, wireframeMaterial);
  }

  initDrawVariables(widthSegments, heightSegments) {
    this.widthSegments = widthSegments;
    this.heightSegments = heightSegments;
  }

  draw(scene) {
    scene.add(this.ellipsoideMesh)
  }

  autoRotateY() {
    this.ellipsoideMesh.rotation.y += 0.001;
  }
}