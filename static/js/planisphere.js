// Three.js ray.intersects with offset canvas
import * as THREE from './lib/three.module.js';
// import { MapControls } from './lib/three/jsm/controls/MapControls.js';
import { OrbitControls } from './lib/three/jsm/controls/OrbitControls.js';
//meracator import
import { WebMercatorMapDefinition } from './simpleWebMercator/mapDefinition.js'
import { Geoservice } from './simpleWebMercator/geoService.js';

// taille de notre canva 
const CANVAS_WIDTH = document.getElementById("planisphere").offsetWidth;
const CANVAS_HEIGHT = document.getElementById("planisphere").offsetHeight;

console.log(document.getElementById("planisphere").offsetHeight)

// récupérer le conteneur div du visuel webgl  
const container = document.getElementById('planisphere');

// initialaisation du visuel webgl 
const renderer = new THREE.WebGLRenderer();
// paramétrage du visuel pour éviter les déformation 
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(CANVAS_WIDTH, CANVAS_HEIGHT);
// ajouter cette visuel du div 
container.appendChild(renderer.domElement);

// initilaisatind e la scene
const scene = new THREE.Scene();
// inilisation de la caméra en perspective camera 
const camera = new THREE.PerspectiveCamera(9, CANVAS_WIDTH / CANVAS_HEIGHT, 0.1, 1000);

// positionement de la caméra
// camera.position.y = 150;
// camera.position.z = 500;
// camera.lookAt(scene.position);
scene.add(camera);

// ajout de lumières 
scene.add(new THREE.AmbientLight(0x222222));
const light = new THREE.PointLight(0xffffff, 1);
camera.add(light);

camera.far = 100000;
camera.updateProjectionMatrix();


// controls

let controls = new OrbitControls(camera, renderer.domElement);
controls.listenToKeyEvents(window); // optional

//controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)

controls.enableDamping = false; // an animation loop is required when either damping or auto-rotation are enabled
// controls.dampingFactor = 0.05;

controls.screenSpacePanning = false;

// controls.minDistance = 100;
// controls.maxDistance = 500;

controls.maxPolarAngle = Math.PI / 2;

let web_mercator = new WebMercatorMapDefinition()

const mapWidth = web_mercator.planelGeometrySize.width;   // Width of the map in pixels
const mapHeight = web_mercator.planelGeometrySize.height;   // Height of the map in pixels
const mapCenter = [0, 0];  // Center point coordinates [longitude, latitude]


const mapMaterial = new THREE.ShaderMaterial({
  vertexShader: `
    varying vec2 vUv;
    void main() {
      vUv = uv;
      gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    }
  `,
  fragmentShader: `
    varying vec2 vUv;
    uniform sampler2D mapTexture;
    void main() {
      vec2 uv = vec2(vUv.x,  vUv.y);  // Flip Y-axis
      gl_FragColor = texture2D(mapTexture, uv);
    }
  `,
  uniforms: {
    mapTexture: { value: null }  // Texture to be set later
  }
});

const mapTexture = new THREE.TextureLoader().load('https://wxs.ign.fr/essentiels/geoportail/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIXSET=PM&TILEMATRIX=0&TILECOL=0&TILEROW=0&STYLE=normal&FORMAT=image/jpeg', () => {
  mapTexture.minFilter = THREE.LinearFilter;
  mapMaterial.uniforms.mapTexture.value = mapTexture;
});

const mapGeometry = new THREE.PlaneGeometry(mapWidth, mapHeight);
const mapMesh = new THREE.Mesh(mapGeometry, mapMaterial);
scene.add(mapMesh);

const geometry = new THREE.SphereGeometry(15, 32, 16);
const material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
const sphere = new THREE.Mesh(geometry, material);

sphere.position.set(0, 0, 0)
// mapMesh.add(sphere)

let web_coord = Geoservice.geographicCoordinateToWebMercatorCartesianCoordianteWithinMap({ longitude: 0, latitude: 0 }, web_mercator.planelGeometrySize)

console.log(web_coord)

const geometry2 = new THREE.SphereGeometry(15, 32, 16);
const material2 = new THREE.MeshBasicMaterial({ color: 0xffff00 });
const sphere2 = new THREE.Mesh(geometry2, material2);

sphere2.position.set(web_coord.x, web_coord.y, 0)
mapMesh.add(sphere2)


mapMesh.position.set(mapWidth / 4, 0, 0);
// mapMesh.rotateZ(Math.PI);

// const controls = new THREE.OrbitControls(camera, renderer.domElement);
camera.position.z = 10000;
camera.position.x = camera.position.x + 500;

function animate() {
  requestAnimationFrame(animate);
  // controls.update();
  renderer.render(scene, camera);
}

animate();