export class WebMercatorMapDefinition {
  EPSG = 3857;
  latituteLimit_deg = 85.0511;
  LongitudeOuestLimit_deg = - 180;
  LatitudeEstLimi_degt = 180;
  web_mercator_radius_convention_ = 6378137

  planelGeometrySize = { width: 2048, height: 2048 };

  constructor() {

  }
}