export class Geoservice {

  static degreeToRadianLatitude(degree_angle) {
    return (degree_angle)(Math.PI / 180)
  }

  static degreeToRadianLlongitude(degree_angle) {
    return (degree_angle)(Math.PI / 180)
  }

  static geographicCoordinateToWebMercatorCartesianCoordianteWithinMap(coordinate, mapHeightWidth) {
    //E = FE + R (λ – λₒ)
    // N = FN + R ln[tan(π/4 + φ/2)
    // mercN
    let x = (coordinate.longitude) * (mapHeightWidth.width / 360);
    // convert from degrees to radians
    let latRad = coordinate.latitude * Math.PI / 180;

    // get y value
    let mercN = (Math.log(Math.tan((90 + coordinate.latitude) * Math.PI / 360)) / (Math.PI / 180))
    let y = (mercN) * (mapHeightWidth.height / 180);
    return { x: x, y: y }
  }

}