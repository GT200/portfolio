---
title: A quick introduction about me.
description: Welcome welcome to my portfolio
# images: ["/images/sample.jpg"]
---

Greetings to you visitor!

I’m Gaëtan TIAZARA, a geomatics engineer with expertise in information systems technology.

In this website, I would like to introduce you to my background, the different skills I acquired during my training and my career.

["Read more on my professional and academic background"](/portfolio/about)