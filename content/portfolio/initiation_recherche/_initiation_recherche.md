---
title: INITIATION A LA RECHERCHE
description: This is the description of our sample project
date: "2022-02-01"
jobDate: 2022
work: [photogrametry]
# techs: [javascript, D3]
# designs: [Photoshop]
thumbnail: images/initiation_recherche/intiation_recherche.png
projectUrl: https://www.sampleorganization.org
---

In order to bring a drone back to the exact location where a photo of old  
postcard was taken and take a photo with the same point of view, we have to set up a toolchain that consists of extracting the absolute position of the center of the perspective of the postcard, orientation and the focal distance from the postcard photo using referenced geometry.
