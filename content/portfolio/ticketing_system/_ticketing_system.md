---
title: Ticketing system
description: web site for ticket reservation
date: "2022-12-10"
jobDate: 2022
work: [Web app, Framework php codeIgniter, reservation system]
#techs: [PHP, codeIgniter, javascript, css, html]
# designs: [Photoshop]
thumbnail: /images/ticketingSystem/ts.gif
projectUrl: https://gitlab.com/GTiazara/ticketingsystem

---

We had to analyze and develop a ticket system for 
a performance hall.

Development languages and tools were free.

Expected user features (minimum):
  -  Be able to buy one or more places;
  - Be able to cancel a reservation;
  -  Ability to choose your seat(s) on a plan.

The developed platform must be capable (at least)
  -  Manage different shows on several dates;
  - Stop sales if a date is complete;
  -  Manage users who buy or cancel seats;
  -  Being able to sell every other spot during Covid-19, for example.