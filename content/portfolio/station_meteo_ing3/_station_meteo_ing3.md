---
title: Connected weather station
description: weather station composed UI (Vue js ), server (express), microcontroller  (raspbery)
date: "2023-02-01"
jobDate: 2023
work: [ weather station composed of UI (Vue js ), server (express), microcontroller  (raspberry)]
# techs: [javascript, D3]
# designs: [Photoshop]
thumbnail: /images/station_meteo_ing3/sondeCentraleMeteo.jpg
projectUrl: https://gitlab.com/kingatsa/oeil-ouragan-project
# testimonial:
#   name: John Doe
#   role: CEO @Example
#   image: sample-project/john.jpg
#   text: Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin
---


This is the dev project of a weather station composed of UI (Vue js ), server (express), microcontroller (raspberry). Each probe of the respberry py is connected to several sensors that give various information such as:

    - Temperature
    -  Hygrometry
    - Atmospheric pressure
    - Rainfall
    - Brightness
    - Wind speed and direction
    - GPS position and time
    - ....

Here is our poject repository and detailed description (team of 2): https://gitlab.com/kingatsa/oeil-ouragan-project

Here is the project instruction: https://ensg_dei.gitlab.io/web-az/js/exercices/projet-station-meteo/



