---
title: MINI-GIS APP
description: development of a desktop gis app
date: "2022-12-15"
jobDate: 2022
work: [DEV AGILE/SCRUM TEAM C++ OPENGL]
# techs: [javascript, D3]
# designs: [Photoshop]
thumbnail: /images/minigis/RuGIS.gif
projectUrl: https://gitlab.com/Jackgeo/MiniGIS/
# testimonial:
#   name: John Doe
#   role: CEO @Example
#   image: sample-project/john.jpg
#   text: Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin
---

## MiniGIS
The master's students of the program Information System Technologies at France's National School of Geographical Sciences - ENSG, were invited to take part in a software development project. The project consists of developing a 2D/3D GIS using C++, Qt and OpenGL.

## Project Description
MiniGIS is a GIS Desktop application capable of displaying, manipulating, and analyzing the various forms of spatial data. MiniGIS supports Vector and Raster data format. The data represented in the application will come from web feeds, existing databases, or files from local or remote repositories.

## My description
During this project, we worked using the scrum framework of the Agile project management method. with a front-end team and a back-end team. The front-end team's main library was OpenGL and the backend team's main library was GDAL OGR. I was part of the back-end team. I learned a lot about GDAL/OGR for handling GIS data. I also gained more experience working in a DevOps team.

## Quick DEMO
[click here to go to the video on gitlab](https://gitlab.com/Jackgeo/MiniGIS/-/blob/dev/RuGIS%20video%20presentation.mp4 )
