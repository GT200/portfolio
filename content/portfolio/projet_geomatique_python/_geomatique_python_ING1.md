---
title: Geomatic project in python
description: Ridge search and talweg in an DTM/DEM raster
date: "2020-10-01"
jobDate: 2022
work: [traitement de données sonnar]
# techs: [javascript, D3]
# designs: [Photoshop]
thumbnail: images/projet_geomatique_python/images_zone_interessante.png
# projectUrl: https://www.sampleorganization.org
# testimonial:
#   name: John Doe
#   role: CEO @Example
#   image: sample-project/john.jpg
#   text: Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin
---

The aim of this project was to detect  ridge  and talweg in a raster of digital terrain model (DTM). The methode used is the simulation of the flow of a drop of water from each pixel of the raster. The points where the drops of water stagnate is the talweg...

