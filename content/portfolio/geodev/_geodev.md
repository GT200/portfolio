---
title: GEODEV
description: This is the description of our sample project
date: "2022-05-15"
jobDate: 2022
work: [Geodetic web app developement with DJANGO]
# techs: [javascript, D3]
# designs: [Photoshop]
thumbnail: /images/geodev/geodev.jpg
projectUrl: http://gt200.pythonanywhere.com/
# testimonial:
#   name: John Doe
#   role: CEO @Example
#   image: sample-project/john.jpg
#   text: Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin
---

In France there is recognised know-how in the field of  geographical science. However, we do not have teaching tools for
understanding the data used and how it helps us
in positioning objects in space, tracking the Earth’s deformations and understanding its internal  structure.

The aim of the project is to develop a graphical interface to visualize and helpunderstand the behaviour of geodetic data.

Like the ICGEM calculation service developed by the German GFZ
(http://icgem.gfz-potsdam.de), this tool have a clear and simple interface
for scientific but above all pedagogical use.