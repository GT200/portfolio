---
title: OpenGL First Project
description: this is my very first openGL project
date: "2022-11-15"
jobDate: 2022
work: [C++ OPENGL 3D scene Creation]
# techs: [javascript, D3]
# designs: [Photoshop]
thumbnail: /images/openGL_first_project/demo.gif
projectUrl: https://gitlab.com/GTiazara/opengltutorial/-/tree/main/DEM_TEST
# testimonial:
#   name: John Doe
#   role: CEO @Example
#   image: sample-project/john.jpg
#   text: Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin
---

The aim of this project was to create a scene using a digital elevation model (DEM) in geotiff format. A camera will represente a player or user in our scene. 

The scene: 
- is textured with one image combined with color using shader GSL language
- has diffuse light modelisation for shading

othe player or user: 
- can go around the 3D DEM scene
- must not go through the scene
- can jump
- rotate

And some 3D objet move in the scene without going out the DEM.
