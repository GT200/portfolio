---
title: GEMINI
description: globe digital twin
date: "2023-05-01"
jobDate: 2023
work: [3D web app based on itowns js framwork]
# techs: [javascript, D3]
# designs: [Photoshop]
thumbnail: /images/gemini_ing3/interface.png
projectUrl: https://github.com/EnzoVenon/Gemini_numeriques/

---

This tool is a prototype of a project supported by IGN, CEREMA and INRIA that wants to create a France Entière Digital Twin. The TSI student project "Digital Gemimi" provides a basis for reflection for this project. The goal is to interconnect different data sources in a tool that will allow to visualize them. The information we view is related to buildings.

Here is the web site to test the tool: https://enzovenon.github.io/Gemini_numeriques/