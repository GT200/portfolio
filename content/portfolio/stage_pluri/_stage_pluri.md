---
title: MULTIDISCIPLINARY INTERNSHIP IN GREECE
description: This is the description of our sample project
date: "2022-08-01"
jobDate: 2022
work: [Sonnar data processing in order to map seagrass]
# techs: [javascript, D3]
# designs: [Photoshop]
thumbnail: images/stage_pluri/stage_pluri.png
projectUrl: https://gitlab.com/GTiazara/seegrass-mapping-at-archipelagos
testimonial:
  name: TIM GrandJean, GIS Supervisor
  #role: GIS Supervisor
  #image: sample-project/john.jpg
  text: "Gaetan Tiazara completed his work placement at Archipelagos Institute at an excellent level, 
showing hard work, initiative and motivation to complete short-term goals, as well as to contribute 
to the wider picture of the conservation efforts of Archipelagos Institute. Gaetan was part of our 
team, reported reguarly and delivered excellent work"
---

Seagrasses are a group of marine plants composed of four families.They play an important role in
aquatic ecosystems such as oxygenation of water, carbon storage, seafloor stabilization, reduction of
current velocity to protect beaches, and organic matter production. They also act as a food source,
and as a spawning ground and refuge for many organisms.

Seagrass degradation has accelerated in recent years and is under constant pressure. Seagrass mead-
ows are very sensitive due to their slow growth rate, vulnerability to anthropogenic impacts and
difficulty recovering once damaged.
For targeted protection of these underwater habitats, low-cost, simple, reliable, accurate and fast
mapping methodology is required. More importantly, publishing these data in international scientific
databases, such as the "European Marine Observation and Data Network" (EMODnet), allows them
to be granted protection from the government.

To map these underwater meadows, the host organisation, Archipelagos, wants to apply several
techniques which are simple, low-cost and reliable to evaluate seagrass distribution. These tech-
niques include: SONAR mapping, drone mapping (mostly as ground truth), SATELLITE mapping
and CITIZEN mapping (study of local fishermen and marine tourists).
The sonar data processing currently used by the organization is a time consuming process and
the result depends on the user capabilities.
Therefore a processing chain that is rigorous, optimised and automatic was set up, allowing the
mapping process of "Posidonia" with sonar data to be faster, more accurate and reliable.
This processing chain was applied to the sonar data around Lipsi Island. The result was then
uploaded to a web mapping interface recently built and also uploaded on EMODnet, the European
Marine Observation and Data Network, a data-center for marine data


## link to the publication site: 
[click here to go to Emodnet](https://www.emodnet-ingestion.eu/submissions/submissions_details.php?menu=39&tpd=1122&step=001seagrass)


<!-- ![Image Title](images/stage_pluri/Rapport_de_stage_provisoire.pdf) -->