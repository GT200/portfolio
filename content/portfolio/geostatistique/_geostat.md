---
title: GEOSTATISTICS, SPATIAL INTERPOLATION
description: This is the description of our sample project
date: "2022-03-01"
jobDate: 2022
work: [Comparing different interpolartion algorithm with django]
# techs: [javascript, D3]
# designs: [Photoshop]
thumbnail: images/geostatistique/kr_lin.png
projectUrl: http://gt200.pythonanywhere.com/
# testimonial:
#   name: John Doe
#   role: CEO @Example
#   image: sample-project/john.jpg
#   text: Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow's nest nipperkin
---

The aim of the project is to represent interpolated surfaces generated from temperature dataset using the interpolation methods that we implemented ouserselves under Python. More over; we had had to play around with different parameter to fine tune the outputs and compare them.
